package com.obviam.starassault;

import com.obviam.starassault.screens.GameScreen;

import com.badlogic.gdx.Game;

public class StarAssault extends Game {

	@Override
	public void create() {
		setScreen(new GameScreen());
	}
}
