package com.example.tic_tac_toe;

import android.os.Bundle;
import android.renderscript.RenderScript;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Point;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Play extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
    	menu.add(0, R.id.new_game, 0, "New Game");
    	menu.add(0, R.id.play_first, 0, "Computer Play First");
    	
        getMenuInflater().inflate(R.menu.activity_play, menu);
        return true;
    }
    
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        
    	switch (item.getItemId()) {
            
    		case R.id.new_game:
                newGame();
                return true;
            
    		case R.id.play_first:
    			newGame();
    			moveFirst();
    			return true;
                
            default:
                return super.onOptionsItemSelected(item);
        }
        
    }
    
    public void newGame(){
    	
    	tick = new TicTacToe();
    	
    	Button btn = (Button)findViewById(R.id.button1);
    	btn.setText("-");
    	
    	btn = (Button)findViewById(R.id.button2);
    	btn.setText("-");
    	
    	btn = (Button)findViewById(R.id.button3);
    	btn.setText("-");
    	
    	btn = (Button)findViewById(R.id.button4);
    	btn.setText("-");
    	
    	btn = (Button)findViewById(R.id.button5);
    	btn.setText("-");
    	
    	btn = (Button)findViewById(R.id.button6);
    	btn.setText("-");
    	
    	btn = (Button)findViewById(R.id.button7);
    	btn.setText("-");
    	
    	btn = (Button)findViewById(R.id.button8);
    	btn.setText("-");
    	
    	btn = (Button)findViewById(R.id.button9);
    	btn.setText("-");
    	
    }
    
    public void messageOnVictory(){
    	
    	AlertDialog.Builder victoryAlert = new AlertDialog.Builder(this);
    	
    	victoryAlert.setTitle("Tic Tac Toe");
    	victoryAlert.setMessage("You Won!");
    	victoryAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				newGame();
			}
		});
    	victoryAlert.setCancelable(false);
    	
    	victoryAlert.create().show();
    }
    
    public void messageOnLoose(){
    	
    	AlertDialog.Builder youLostMessage = new AlertDialog.Builder(this);
    	
    	youLostMessage.setTitle("Tic Tac Toe");
    	youLostMessage.setMessage("You Lost.");
    	youLostMessage.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				newGame();
			}
		});
    	youLostMessage.setCancelable(false);
    	
    	youLostMessage.create().show();
    }
    
    public void messageOnDraw(){
    	
    	AlertDialog.Builder gameOverMessage = new AlertDialog.Builder(this);
    	
    	gameOverMessage.setTitle("Tic Tac Toe");
    	gameOverMessage.setMessage("Its a Draw.");
    	gameOverMessage.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				newGame();
			}
		});
    	gameOverMessage.setCancelable(false);
    	
    	gameOverMessage.create().show();
    }
    
    public TicTacToe tick = new TicTacToe();
    
    public void move(View view){
    	
    	
    	Button btn = (Button)view;
    	Point move_pos = buttonToIndex( btn.getTag().toString() );
    	
    	btn.setText("O");
    	
    	tick.move(move_pos.x, move_pos.y);
    	
    	int[][] currPosition = tick.copyBoard();
    	
    	
    	/*check whether the human had won*/
    	if( tick.victory(currPosition, 1) ){
    		
    		messageOnVictory();
    		return;
    	}
    	
    	/*check whether the game is over*/
    	if( tick.gameOver(currPosition) ){
    		
    		messageOnDraw();
    		return;
    	}
    	
    	int bestX = 0;
    	int bestY = 0;
    	double bestVal = -100000000;
    	
    	boolean hasGoodMove = false;
    	
    	int weight = 100000000;
    	
    	for(int x=0; x < 3; x++)
    		for(int y=0; y < 3; y++)
    			if( currPosition[x][y] == 0){
    				
    				currPosition[x][y] = 2;
    				double val = tick.thinkHuman(currPosition, 0, weight);
    				
    				if(val > bestVal){
    					
    					bestVal = val;
    					bestX = x;
    					bestY = y;
    					
    					hasGoodMove = true;
    				}
    				
    				currPosition = tick.copyBoard();
    			}
    	
    	if( hasGoodMove ){
    		
    		tick.computerMove(bestX, bestY);
    		
    		paintMove(bestX, bestY);
    		
    	}
    	
    	/*check whether the computer had won*/
    	if( tick.victory( tick.copyBoard(), 2) ){
    		
    		messageOnLoose();
    		return;
    	}
    	
    	/*check whether the game is over*/
    	if( tick.gameOver( tick.copyBoard() ) ){
    		
    		messageOnDraw();
    		return;
    	}
    	
    }
    
    public void moveFirst(){
    	
    	int[][] currPosition = tick.copyBoard();
    	
    	int bestX = 0;
    	int bestY = 0;
    	double bestVal = -10000000;
    	
    	boolean hasGoodMove = false;
    	
    	int weight = 10000000;
    	
    	for(int x=0; x < 3; x++)
    		for(int y=0; y < 3; y++)
    			if( currPosition[x][y] == 0){
    				
    				currPosition[x][y] = 2;
    				double val = tick.thinkHuman(currPosition, 0, weight);
    				
    				if(val > bestVal){
    					
    					bestVal = val;
    					bestX = x;
    					bestY = y;
    					
    					hasGoodMove = true;
    				}
    				
    				currPosition = tick.copyBoard();
    			}
    	
    	if( hasGoodMove ){
    		
    		tick.computerMove(bestX, bestY);
    		
    		paintMove(bestX, bestY);
    	}	
    	
    }
    
    public Point buttonToIndex(String tag){
    	
    	Point p = new Point(0,0);
    	
    	if( tag.equals("2") )
    		p = new Point(1,0);
    	
    	if( tag.equals("3") )
    		p = new Point(2,0);
    	
    	if( tag.equals("4") )
    		p = new Point(0,1);
    	
    	if( tag.equals("5") )
    		p = new Point(1,1);
    	
    	if( tag.equals("6") )
    		p = new Point(2,1);
    	
    	if( tag.equals("7") )
    		p = new Point(0,2);
    	
    	if( tag.equals("8") )
    		p = new Point(1,2);
    	
    	if( tag.equals("9") )
    		p = new Point(2,2);
    	
    	
    	return p;
    }
    
    public void paintMove(int x, int y){/* reflects the move on the display*/
    	
    	Button btn = (Button)findViewById(R.id.button1);
    	
    	if( (x == 0) && (y == 0) )
    		btn = (Button)findViewById(R.id.button1);
    	
    	if( (x == 1) && (y == 0) )
    		btn = (Button)findViewById(R.id.button2);
    	
    	if( (x == 2) && (y == 0) )
    		btn = (Button)findViewById(R.id.button3);
    	
    	if( (x == 0) && (y == 1) )
    		btn = (Button)findViewById(R.id.button4);
    	
    	if( (x == 1) && (y == 1) )
    		btn = (Button)findViewById(R.id.button5);
    	
    	if( (x == 2) && (y == 1) )
    		btn = (Button)findViewById(R.id.button6);
    	
    	if( (x == 0) && (y == 2) )
    		btn = (Button)findViewById(R.id.button7);
    	
    	if( (x == 1) && (y == 2) )
    		btn = (Button)findViewById(R.id.button8);
    	
    	if( (x == 2) && (y == 2) )
    		btn = (Button)findViewById(R.id.button9);
    	
    	btn.setText("X");
    	
    }
    
    
}
