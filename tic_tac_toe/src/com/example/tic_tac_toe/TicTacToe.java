package com.example.tic_tac_toe;

public class TicTacToe {
	
	private int[][] board = new int[3][3];
	
	public TicTacToe(){
		
		for(int x=0; x < 3; x++)
			for(int y=0; y < 3; y++)
				this.board[x][y] = 0;
		
	}
	
	/* O = 1 and X = 2 : humans are O and computer is X */
	
	public boolean  victory(int[][] position,int marker){/* marker = 1 for human and 2 for computer */
		
		/*search in rows*/
		for(int y=0; y < 3; y++)
			if( (position[0][y] == marker) && (position[1][y] == marker) && (position[2][y] == marker)  )
				return true;
		
		/*search in columns*/
		for(int x=0; x < 3; x++)
			if( (position[x][0] == marker) && (position[x][1] == marker) && (position[x][2] == marker)  )
				return true;
		
		/*search diagonals*/
		if( (position[0][0] == marker) && (position[1][1] == marker) && (position[2][2] == marker)  )
			return true;
		
		if( (position[2][0] == marker) && (position[1][1] == marker) && (position[0][2] == marker)  )
			return true;
		
		return false;
	}
	
	public boolean gameOver(int[][] position){
		
		for(int x=0; x < 3; x++)
			for(int y=0; y < 3; y++)
				if(position[x][y] == 0)
					return false;
		
		return true;
	}
	
	private int[][] copyPosition(int[][] position){
		
		int[][] duplicate = new int[3][3];
		
		
		for(int x=0; x < 3; x++)
			for(int y=0; y < 3; y++)
				
				if(position[x][y] == 1)
					duplicate[x][y] = 1;
				
				else if(position[x][y] == 2)
					duplicate[x][y] = 2;
				
				else
					duplicate[x][y] = 0;
		
		
		return duplicate;
	}
	
	public double think(int[][] position, double currScore, int weight){/* value if higher if the position is favorable to the computer*/
		
		int value = 0;
		
		if( this.victory(position, 2) )/* if computer had won */
			return currScore + (1 * weight);
		
		if( this.victory(position, 1) )/* human had won*/
			return currScore - (1 * weight);
		
		if( this.gameOver(position) )/* game is a draw*/
			return currScore;
		
		/*weight /= 10;*/
		
		int num_moves = 0;
		for(int x=0; x < 3; x++)
			for(int y=0; y < 3; y++)
				
				if(position[x][y] == 0){/* square is free*/
					
					int[][] duplicatePosition = this.copyPosition(position);
					duplicatePosition[x][y] = 2; /* put a 'X' in the empty square*/
					value += thinkHuman(duplicatePosition, currScore, weight);
					
					num_moves++;
				}
		
		return ( (double)value / (double)num_moves);
	}
	
	public double thinkHuman(int[][] position, double currScore, int weight){/* value if higher if the position is favorable to the computer*/
		
		double value = 0;
		
		if( this.victory(position, 2) )/* if computer had won */
			return currScore + (1 * weight);
		
		if( this.victory(position, 1) )/* human had won*/
			return currScore - (1 * weight);
		
		if( this.gameOver(position) )/* game is a draw*/
			return currScore;
		
		/*weight /= 10;*/
		double bestVal = 0;
		int bestX = 0;
		int bestY = 0;
		
		int num_moves = 0;
		for(int x=0; x < 3; x++)
			for(int y=0; y < 3; y++)
				
				if(position[x][y] == 0){/* square is free*/
					
					int[][] duplicatePosition = this.copyPosition(position);
					duplicatePosition[x][y] = 1; /* put a 'X' in the empty square*/
					value = think(duplicatePosition, currScore, weight);
					
					if(value < bestVal){
						
						bestX = x;
						bestY = y;
						bestVal = value;
					}
					
					num_moves++;
				}
		
		return bestVal;
	}
	
	public void move(int x, int y){
		
		
		this.board[x][y] = 1;
	}
	
	public void computerMove(int x, int y){
		
		this.board[x][y] = 2;
	}
	
	public int[][] copyBoard(){
		
		int[][] copy = this.copyPosition(this.board);
		
		return copy;
	}
	
}
