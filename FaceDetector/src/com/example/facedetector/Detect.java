package com.example.facedetector;

import android.media.FaceDetector;
import android.media.FaceDetector.Face;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.Menu;
import android.view.View.MeasureSpec;
import android.widget.ImageView;
import android.widget.TextView;

public class Detect extends Activity {
	
	
	private static final int MAX_FACES = 5;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detect);
		
		loadImage();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_detect, menu);
		return true;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		
		if(requestCode == 1)
			if(resultCode == RESULT_OK){
				
				ImageView img = (ImageView)findViewById(R.id.imageView1);
				img.setImageURI( data.getData() );
				
				detectFaces();
			}
		
	}
	
	public void loadImage(){
		
		Intent galleryIntent = new Intent();
		galleryIntent.setDataAndType(Uri.parse("content://downloads/external/images/media"), "image/*");
		galleryIntent.setAction(Intent.ACTION_PICK);
		
		startActivityForResult(galleryIntent, 1);
		
	}
	
	
	
	public void detectFaces(){
		
		
		ImageView img = (ImageView)findViewById(R.id.imageView1);
		
		Bitmap bmp = Detect.getBitmap(img);
		
		int width = bmp.getWidth();
		int height = bmp.getHeight();
		
		FaceDetector detector = new FaceDetector(width, height, Detect.MAX_FACES);
		Face faces[] = new Face[ Detect.MAX_FACES ];
		
		
		Bitmap bitmap565 = Bitmap.createBitmap(width, height, Config.RGB_565);
        Paint ditherPaint = new Paint();
        Paint drawPaint = new Paint();
         
        ditherPaint.setDither(true);
        drawPaint.setColor(Color.RED);
        drawPaint.setStyle(Paint.Style.STROKE);
        drawPaint.setStrokeWidth(2);
         
        Canvas canvas = new Canvas();
        canvas.setBitmap(bitmap565);
        canvas.drawBitmap(bmp, 0, 0, ditherPaint);
         
        int facesFound = detector.findFaces(bitmap565, faces);
		
		TextView txt = (TextView)findViewById(R.id.textView1);
		
		Integer found = facesFound;
		String fn = found.toString();
		
		txt.setText( fn );
	}
	
	private static Bitmap getBitmap(ImageView v){
		
		v.setDrawingCacheEnabled(true);

		// this is the important code :)  
		// Without it the view will have a dimension of 0,0 and the bitmap will be null          
		v.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED), 
		            MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
		v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight()); 

		v.buildDrawingCache(true);
		Bitmap b = Bitmap.createBitmap(v.getDrawingCache());
		v.setDrawingCacheEnabled(false); // clear drawing cache
		
		return b;
	}
	
	

}
