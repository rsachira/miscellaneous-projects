package com.randezvou.app.slice;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.WorldManifold;
import com.randezvou.app.joints.RevoluteJoint;

public class MyGdxGame implements ApplicationListener, ContactListener, InputProcessor {
	
	static final float WORLD_TO_BOX = 0.01f;
	static final float BOX_WORLD_TO = 100f;
	static final float BOX_STEP = 1/60f;
    static final int BOX_VELOCITY_ITERATIONS = 6;
    static final int BOX_POSITION_ITERATIONS = 2;
    static final boolean FOLLOW = true;
    static final float SCREEN_HEIGHT = 768;
	
	World world;
	Box2DDebugRenderer debugRenderer;
	ShapeRenderer shapeRenderer;
	
	Vector2 A;
	Vector2 B, B1, B2;
	
	float resX, resY = 0;
	
	Vector2 point1 = new Vector2();
	
	Body body2;
	
	boolean drawLine = false;
	boolean collided = false;
	
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Texture texture;
	private Sprite sprite;
	
	private Vector2 ballVelocity;
	
	@Override
	public void create() {
		
		world = new World(new Vector2(0, -100), true);
		world.setContactListener(this);
		
		camera = new OrthographicCamera();
		camera.viewportHeight = 320;
        camera.viewportWidth = 480;
        
        camera.position.set(camera.viewportWidth * .5f, camera.viewportHeight * .5f, 0f);  
        camera.update();
        
        resX = Gdx.graphics.getWidth();
		resY = Gdx.graphics.getHeight();
        
        debugRenderer = new Box2DDebugRenderer();
		batch = new SpriteBatch();
		shapeRenderer = new ShapeRenderer();
		
		Body body = Box2DHelper.createBody(world, new Vector2(camera.viewportWidth * .5f - 200, (camera.viewportHeight * .5f) + 20), 0.0f, BodyType.DynamicBody);
		
		Vector2[] vertices = new Vector2[5];
		vertices[0] = new Vector2(0, 0);
		vertices[1] = new Vector2(100, 0);
		vertices[2] = new Vector2(100, 100);
		vertices[3] = new Vector2(50, 150);
		vertices[4] = new Vector2(0, 100);
		
		Shape bodyShape = Box2DHelper.circle(30.0f);
		Fixture f1 = Box2DHelper.createFixture(body, bodyShape, 1.0f, 1.0f, 0.0f);
		f1.setUserData(new String("circle"));
		
		//body.applyLinearImpulse(new Vector2(5000000, 50000), body.getPosition(), true);
		
		
		body2 = Box2DHelper.createBody(world, new Vector2(camera.viewportWidth * .5f, camera.viewportHeight * .5f), 0.0f, BodyType.StaticBody);
		
		RevoluteJoint j = new RevoluteJoint(body, body2, true);
		j.SetAnchorA(body.getPosition().x, body.getPosition().y);
		j.SetAnchorB(100, 100);
		j.CreateJoint(world);
		
		float centerX = camera.viewportWidth * .5f;
		float centerY =  camera.viewportHeight * .5f;
		
		A = new Vector2(centerX, centerY + 60);
		B = new Vector2(centerX, centerY - 60);
		
		B1 = new Vector2(0, 0);
		B2 = new Vector2(0, 0);
		
		//drawLine = true;
		
		Shape body2Shape = Box2DHelper.box(100, 100);
		Fixture f2 = Box2DHelper.createFixture(body2, body2Shape, 1.0f, 1.0f, 0.0f);
		f2.setUserData(new String("box"));
		
		//SplitEngine.splitObj(world, body2, A, B);
		
		//Gdx.input.setInputProcessor(this);
	}

	@Override
	public void dispose() {
		
	}

	@Override
	public void render() {		
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		world.step(BOX_STEP, BOX_VELOCITY_ITERATIONS, BOX_POSITION_ITERATIONS);
		
		if(drawLine){
        	
        	shapeRenderer.setProjectionMatrix(camera.combined);
        	shapeRenderer.begin(ShapeType.Line);
        	shapeRenderer.setColor(0, 0, 0, 0);
        	shapeRenderer.line(A, B);
        	shapeRenderer.line(A, B1);
        	shapeRenderer.line(A, B2);
        	shapeRenderer.end();
        }
		
		
		
		if(collided){
			
			
			breakDown(A, B);
			breakDown(A, B1);
			breakDown(A, B2);
			
			collided = false;
		}
		
		
		
		
		debugRenderer.render(world, camera.combined);
	}
	
	private void breakDown(Vector2 a, Vector2 b){
		
		world.rayCast(new RayCastCallback() {
			
			@Override
			public float reportRayFixture(Fixture fixture, Vector2 point,
					Vector2 normal, float fraction) {
				// TODO Auto-generated method stub
				
				String s = (String)fixture.getUserData();
				
				if( !s.equals("box") )
					return 1;
				
				point1.x = point.x;
				point1.y = point.y;
				
				return 0;
			}
		}, a, b);
		
		world.rayCast(new RayCastCallback() {
			
			@Override
			public float reportRayFixture(Fixture fixture, Vector2 point,
					Vector2 normal, float fraction) {
				// TODO Auto-generated method stub
				
				String s = (String)fixture.getUserData();
				
				if( !s.equals("box") )
					return 1;
				
				SplitEngine.splitObj(world, fixture.getBody(), point1, point, ballVelocity);
				world.destroyBody(fixture.getBody());
				
				return 0;
			}
		}, b, a);
	}
	
	public float gameX(float screenX){
		
		float center_theoretical = resX / 2;
		float center_practical = camera.position.x;
		
		return screenX * (center_practical / center_theoretical);
	}
	
	public float gameY(float screenY){
		
		float center_theoretical = resY / 2;
		float center_practical = camera.position.y;
		
		return screenY * (center_practical / center_theoretical);
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void beginContact(Contact contact) {
		// TODO Auto-generated method stub
		
		Fixture fixA = contact.getFixtureA();
		Fixture fixB = contact.getFixtureB();
		
		WorldManifold wm = contact.getWorldManifold();
		Vector2 point0 = new Vector2(wm.getPoints()[0].x, wm.getPoints()[0].y);
		
		String userA = (String)fixA.getUserData();
		String userB = (String)fixB.getUserData();
		
		double deltaAngle = Math.PI / 180 * 10;
		
		if(userA.equals("circle") && userB.equals("box") ){
			
			ballVelocity = fixA.getBody().getLinearVelocity();
			
			A.x = point0.x;
			A.y = point0.y;
			
			Vector2 velocity = fixA.getBody().getLinearVelocity();
			
			double direction = (float) Math.atan2( velocity.y, (double)velocity.x);
			
			B.x = A.x + 200 * (float)Math.cos(direction);
			B.y = A.y + 200 * (float)Math.sin(direction);
			
			B1.x = A.x + 200 * (float)Math.cos(direction + deltaAngle);
			B1.y = A.y + 200 * (float)Math.sin(direction + deltaAngle);
			
			B2.x = A.x + 200 * (float)Math.cos(direction - deltaAngle);
			B2.y = A.y + 200 * (float)Math.sin(direction - deltaAngle);
			
			drawLine = true;
			collided = true;
		}
		
		if(userB.equals("circle") && userA.equals("box") ){
			
			ballVelocity = fixB.getBody().getLinearVelocity();
			
			A.x = point0.x;
			A.y = point0.y;
			
			Vector2 velocity = fixB.getBody().getLinearVelocity();
			
			double direction = (float) Math.atan2( velocity.y, (double)velocity.x);
			
			B.x = A.x + 200 * (float)Math.cos(direction);
			B.y = A.y + 200 * (float)Math.sin(direction);
			
			B1.x = A.x + 200 * (float)Math.cos(direction + deltaAngle);
			B1.y = A.y + 200 * (float)Math.sin(direction + deltaAngle);
			
			B2.x = A.x + 200 * (float)Math.cos(direction - deltaAngle);
			B2.y = A.y + 200 * (float)Math.sin(direction - deltaAngle);
			
			drawLine = true;
			collided = true;
		}
		
	}

	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		
		if( !drawLine ){
			
			A = new Vector2( gameX(screenX), gameY(resY - screenY) );
			B = new Vector2( gameX(screenX), gameY(resY - screenY) );
			drawLine = true;
			
		}else{
			
			world.rayCast(new RayCastCallback() {
				
				@Override
				public float reportRayFixture(Fixture fixture, Vector2 point,
						Vector2 normal, float fraction) {
					// TODO Auto-generated method stub
					
					String s = (String)fixture.getUserData();
					
					if( !s.equals("box") )
						return 1;
					
					point1.x = point.x;
					point1.y = point.y;
					
					return 0;
				}
			}, A, B);
			
			world.rayCast(new RayCastCallback() {
				
				@Override
				public float reportRayFixture(Fixture fixture, Vector2 point,
						Vector2 normal, float fraction) {
					// TODO Auto-generated method stub
					
					String s = (String)fixture.getUserData();
					
					if( !s.equals("box") )
						return 1;
					
					SplitEngine.splitObj(world, fixture.getBody(), point1, point, ballVelocity);
					world.destroyBody(fixture.getBody());
					
					return 0;
				}
			}, B, A);
			
		}
		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		
		B = new Vector2( gameX(screenX), gameY(resY - screenY) );
		
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}
