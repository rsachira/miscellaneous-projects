package com.me.mygdxgame;

import java.util.ArrayList;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class MyGame implements Screen, InputProcessor, ContactListener {
	
	
	World world;
	Box2DDebugRenderer debugRenderer;
	
	static final float WORLD_TO_BOX = 0.01f;
	static final float BOX_WORLD_TO = 100f;
	static final float BOX_STEP = 1/60f;
    static final int BOX_VELOCITY_ITERATIONS = 6;
    static final int BOX_POSITION_ITERATIONS = 2;
    static final boolean FOLLOW = true;
    static final float SCREEN_HEIGHT = 768;
    
    public static Ball ball;
    public static Ball balloony;
	
	public static OrthographicCamera camera;
	
	private SpriteBatch batch;
	public static ArrayList<SpriteWrapper> sprites = new ArrayList<SpriteWrapper>();
	public static ArrayList<TouchButton> touchButtons = new ArrayList<TouchButton>();
	public static ArrayList<FlyingTile> flyingTiles = new ArrayList<FlyingTile>();
	public static ArrayList<OrdinaryCoin> coins = new ArrayList<OrdinaryCoin>();
	public static ArrayList<Sprite[]> grassSprites = new ArrayList<Sprite[]>();
	
	//public static Texture groundTileTex;
	//public static Texture coinTex;
	
	private Vector2 fingerInitial;
	private MapBuilder maps;
	
	private boolean timer_start;
	private float elapsed_time;
	private float time;
	private TouchButton jumpBtn;
	
	private Sound boing;
	
	enum GameState{
		
		WON, LOST, RUNNING, MENU
	}
	
	GameState state = GameState.MENU;
	
	public static ArrayList<Body> deleteList = new ArrayList<Body>();
	
	Background bg1;
	Background bg2;
	
	private int levelNo;
	public static int totalCoins = 0;
	public static float lastTouchDown = 0;
	
	private Game game;

	public MyGame(Game g, int levelNo){
		
		game = g;
		this.levelNo = levelNo;
		state = GameState.RUNNING;
	}
	
	//Grass grass;
	ShapeRenderer sr = new ShapeRenderer();
	boolean drawShape = false;
	float X1, Y1, X2, Y2 = 0.0f;
	float preCameraX = 0;
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		
		if(state == GameState.MENU){
			
			//batch.setProjectionMatrix(camera.combined);
			//menuScreen.render(batch);
			//return;
		}
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		
        world.step(BOX_STEP, BOX_VELOCITY_ITERATIONS, BOX_POSITION_ITERATIONS);
        
        for(int i=0; i < deleteList.size(); i++){
        	
        	Body body = deleteList.get(i);
        	
        	if(body == null)
        		continue;
        	
        	world.destroyBody(body);
        	deleteList.remove(i);
        }
        
        if(FOLLOW && shouldFollow(ball.ballBody.getLinearVelocity(), ball.ballBody.getPosition(), camera.position.x) ){
        	camera.position.set( ball.ballBody.getPosition().x, camera.viewportHeight * .5f, 0f);
        	camera.update();
        }
        
        //Move background to create a slower background moving speed(relative to the camera)
        float deltaCameraX = camera.position.x - preCameraX;
        bg1.move(deltaCameraX / 5);
        preCameraX = camera.position.x;
        
        //check whether lost
        if(ball.ballBody.getPosition().y < -ball.width)
        	state = GameState.LOST;
        
        if(state == GameState.LOST){
        	
        	// if lost restart the game
        	restart();
        }
        
        if(state == GameState.WON){
        	
        	game.setScreen(new WonScreen(game, ball.getScore() + getTimeBonus(time), MyGame.totalCoins, levelNo));
        }
        
        batch.setProjectionMatrix(camera.combined);
        
        batch.enableBlending();
        batch.begin();
        
        bg1.update(camera.position.x - (camera.viewportWidth / 2) );
        bg1.getBackSprite().draw(batch);
        bg1.getNextBack().draw(batch);
        
        MyGdxGame.tweenManager.update(Gdx.graphics.getDeltaTime());
        
        // Draw Grass
        for(int i = 0; i < grassSprites.size(); i++)
        	for(int j = 0; j < 3; j++)
        		grassSprites.get(i)[j].draw(batch);
        
        batch.end();
        
        ball.ballSprite.setPosition(ball.ballBody.getPosition().x - (ball.width / 2), ball.ballBody.getPosition().y - (ball.width / 2) );
        balloony.ballSprite.setPosition(balloony.ballBody.getPosition().x - (balloony.width / 2), balloony.ballBody.getPosition().y - (balloony.width / 2) );
        
		batch.enableBlending();
        batch.begin();
        
        ball.blink();
        
        for(int i=0; i < sprites.size(); i++)
        	if(sprites.get(i).shouldDraw){
        		
        		ArrayList<Sprite> collection = sprites.get(i).spriteCollection;
        		
        		for(int j = 0; j < collection.size(); j++)
        			collection.get(j).draw(batch);
        	}
        
        
        for(int i = 0; i < flyingTiles.size(); i++)
        	flyingTiles.get(i).update();
        
        drawScore(ball.getScore());
        
        //Draw elapsed time
        if(timer_start)
        	time += Gdx.graphics.getDeltaTime();
        
        drawTime(time);
        
        for(int i = 0; i < touchButtons.size(); i++){
        	
        	ArrayList<Sprite> collection = touchButtons.get(i).sw.spriteCollection;
    		
    		for(int j = 0; j < collection.size(); j++)
    			collection.get(j).draw(batch);
        }
        
        batch.end();
        
        if(drawShape){
        	
        	sr.setProjectionMatrix(camera.combined);
        	sr.begin(ShapeType.Line);
        	sr.setColor(0, 0, 0, 0);
        	sr.line(X1, Y1, X2, Y2);
        	sr.end();
        }
        
        //debugRenderer.render(world, camera.combined);
	}
	
	private void restart(){	// restart the game
		
		dispose();
    	
    	state = GameState.RUNNING;
    	show();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}
	
	FlyingTile fl;
	RayCastCallback  intersection;
	
	@Override
	public void show() {
		
		// Setup world
		world = new World(new Vector2(0, -100), true);
		world.setContactListener(this);
		
		// Setup camera
		camera = new OrthographicCamera();
		camera.viewportHeight = 320;
        camera.viewportWidth = 480;
        camera.position.set(camera.viewportWidth * .5f, camera.viewportHeight * .5f, 0f);
        camera.update();
        
        // Initialize variables
        sprites = new ArrayList<SpriteWrapper>();
        flyingTiles = new ArrayList<FlyingTile>();
        MyGame.totalCoins = 0;
        TweenCallBacks.coinsBouncing = false;
        timer_start = false;
        time = 0;
        elapsed_time = 0;
        preCameraX = camera.position.x;
        TouchButton jumpBtn = new TouchButton(camera.viewportWidth - 50, 50, 20, 20, world, new Texture(Gdx.files.internal("data/ball_new.png"))); //Jump button
        touchButtons.add(jumpBtn);
        
        Tween.registerAccessor(OrdinaryCoin.class, new CoinAccessor());
        Tween.registerAccessor(Sprite.class, new GrassSpriteAccessor());
        
        batch = new SpriteBatch();
		debugRenderer = new Box2DDebugRenderer();
		
		TextureRegion backTexRegion = new TextureRegion( (Texture)MyGdxGame.manager.get(MyGdxGame.BACKGROUND_1), 2048, 800);
		
        bg1 = new Background(0, 0, 819.2f, 320, backTexRegion);
        
        boing = Gdx.audio.newSound(Gdx.files.internal("data/sounds/Boing.mp3"));
        
        MyGdxGame.font.setScale(1, 1);
        MyGdxGame.font.scale(-0.75f);
        MyGdxGame.font.setColor(Color.BLACK);
        
        if(!Gdx.files.internal("data/levels/" + Integer.toString(levelNo) + ".txt").exists()){
        	
        	game.setScreen(new MissionScreen(game));
        	return;
        }
        
        maps = new MapBuilder(world);
        maps.loadMap("data/levels/" + Integer.toString(this.levelNo) + ".txt");
        
		Gdx.input.setInputProcessor(this);
	}
	
	private void drawScore(int score){
		
		String strScore = Integer.toString(score);
		
		float textWidth = MyGdxGame.font.getBounds(strScore).width;
		
		float x = camera.position.x + (camera.viewportWidth / 2) - textWidth - 5;
		float y = camera.position.y + (camera.viewportHeight / 2) + 10;
		
		MyGdxGame.font.draw(batch, strScore, x, y);
		
	}
	
	private void drawTime(float time){
		
		String strTime = Float.toString(time);
		
		float x = camera.position.x - (camera.viewportWidth / 2) + 5;
		float y = camera.position.y + (camera.viewportHeight / 2) + 10;
		
		MyGdxGame.font.draw(batch, strTime, x, y);
	}
	
	private int getTimeBonus(float timeElapsed){
		
		double bonus = 0;
		bonus = Math.pow(1.02, -1 * timeElapsed);
		bonus *= 100;
		
		return (int)bonus;
	}
	
	private boolean shouldFollow(Vector2 ballVelocity, Vector2 ballPos, float cameraX){
		
		if( ((ballVelocity.x > 0) && (ballPos.x >= cameraX)) || ((ballVelocity.x < 0) && (ballPos.x <= cameraX) && (camera.position.x > (camera.viewportWidth / 2) ) ) )
				return true;
		
		return false;
	}
	
	public float gameX(float screenX){
		
		float halfWidth = 240;
		
		float center_theoretical = MyGdxGame.resX / 2;
		float center_practical = camera.position.x;
		
		return ( screenX * (center_practical / center_theoretical) ) + (camera.position.x - halfWidth);
		
		
		//return (2 * camera.position.x) / 480 * screenX;
		
	}
	
	public float gameY(float screenY){
		
		float center_theoretical = MyGdxGame.resY / 2;
		float center_practical = camera.position.y;
		
		return screenY * (center_practical / center_theoretical);
	}
	
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
		dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		restart();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
		batch.dispose();
		boing.dispose();
		sprites = new ArrayList<SpriteWrapper>();
		deleteList = new ArrayList<Body>();
		flyingTiles = new ArrayList<FlyingTile>();
		coins = new ArrayList<OrdinaryCoin>();
		grassSprites = new ArrayList<Sprite[]>();
		MyGdxGame.tweenManager.killAll();
		MyGdxGame.tweenManager = new TweenManager();
		TweenCallBacks.coinsBouncing = false;
		TweenCallBacks.grassShaking = false;
		world.dispose();
	}

	@Override
	public void beginContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
		Fixture A = contact.getFixtureA();
		Fixture B = contact.getFixtureB();
		
		if(A.getUserData() != null && B.getUserData() != null){
			
			UserData userA = (UserData)A.getUserData();
			UserData userB = (UserData)B.getUserData();
			
			if(userA.objType == UserData.Type.BALL && userB.objType == UserData.Type.COIN){
				
				
				if(sprites.get(userB.spriteIndex).shouldDraw == false){
					
					contact.setEnabled(false);
					return;
				}
				
				
				ball.setScore(ball.getScore() + userB.value);
				OrdinaryCoin.shouldRemove(userB.spriteIndex);
				//removeAnimation(userB.spriteIndex);
				//deleteList.add(B.getBody());
				contact.setEnabled(false);
				
				sprites.get(userB.spriteIndex).shouldDraw = false;
				
				return;
			}
			
			if(userB.objType == UserData.Type.BALL && userA.objType == UserData.Type.COIN){
				
				
				if(sprites.get(userA.spriteIndex).shouldDraw == false){
					
					contact.setEnabled(false);
					return;
				}
				
				ball.setScore(ball.getScore() + userA.value);
				OrdinaryCoin.shouldRemove(userA.spriteIndex);
				//removeAnimation(userA.spriteIndex);
				//deleteList.add(A.getBody());
				contact.setEnabled(false);
				
				sprites.get(userA.spriteIndex).shouldDraw = false;
				
				return;
			}
			
			if(userA.objType == UserData.Type.BALL && userB.objType == UserData.Type.BALL){
				
				timer_start = false;
				state = GameState.WON;
			}
			
			if( (userA.objType == UserData.Type.BALL && userB.objType == UserData.Type.TILE && withinCurrentFrame(A.getBody().getPosition()) ) || (userA.objType == UserData.Type.TILE && userB.objType == UserData.Type.BALL && withinCurrentFrame(B.getBody().getPosition())) ){
				
				boing.play();
			}
			
		}
		
	}
	
	public boolean withinCurrentFrame(Vector2 pos){
		
		if( (camera.position.x + (camera.viewportWidth / 2) ) >= pos.x && (camera.position.x - (camera.viewportWidth / 2) ) <= pos.x )
			if( (camera.position.y + (camera.viewportHeight / 2) ) >= pos.y && (camera.position.y - (camera.viewportHeight / 2) ) <= pos.y )
				
				return true;
		
		return false;
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		
		//System.out.println( gameX(screenX) );
		
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
