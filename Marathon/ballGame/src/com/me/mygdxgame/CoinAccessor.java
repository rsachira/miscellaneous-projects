package com.me.mygdxgame;

import aurelienribon.tweenengine.TweenAccessor;

public class CoinAccessor implements TweenAccessor<OrdinaryCoin>{

	@Override
	public int getValues(OrdinaryCoin target, int tweenType, float[] returnValues) {
		// TODO Auto-generated method stub
		
		returnValues[0] = target.c.coinBody.getPosition().y;
		
		return 1;
	}

	@Override
	public void setValues(OrdinaryCoin target, int tweenType, float[] newValues) {
		// TODO Auto-generated method stub
		
		float x = target.c.coinBody.getPosition().x;
		float y = target.c.coinBody.getPosition().y;
		
		target.c.coinBody.setTransform(x, newValues[0], 0);
		
		SpriteWrapper sw = MyGame.sprites.get(target.spritesIndex);
		float deltaY = newValues[0] - y;
		
		if(sw.shouldDraw)
			for(int i = 0; i < sw.spriteCollection.size(); i++)
				sw.spriteCollection.get(i).translate(0, deltaY);
		
	}
	
	
}
