package com.me.mygdxgame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class ButtonInput extends InputListener{
	
	private Game game;
	private int levelNo;
	
	public ButtonInput(Game g, int level){
		
		game = g;
		levelNo = level;
	}
	
	@Override
	public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
		// TODO Auto-generated method stub
		
		game.setScreen(new MyGame(game, levelNo));
		
		return true;
	}
}
