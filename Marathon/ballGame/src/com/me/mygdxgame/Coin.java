package com.me.mygdxgame;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;

public class Coin {
	
	private int value;
	public Body coinBody;
	private Shape coinShape;
	
	public static final float radius = 12.5f;
	
	public Coin(World world, float x, float y, int coinValue, int index){
		
		value = coinValue;
		
		coinBody = Box2DHelper.createBody(world, new Vector2(x, y), 0, BodyType.StaticBody);
		coinShape = Box2DHelper.circle(radius);
		Fixture coinFixture = Box2DHelper.createFixture(coinBody, coinShape, 1.0f, 0.0f, 0.0f);
		
		UserData us = new UserData(UserData.Type.COIN, value, index);
		coinFixture.setUserData(us);
	}
	
	public void update(float deltaT){
		
		float a = 10;	// amplitude;
		float f = 50;	// frequency = 50Hz
		
		float w = 2 * (float)Math.PI * f;
		
		float deltaH = a * (float)Math.sin(w * deltaT);
	}
	
}