package com.me.mygdxgame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SkinLoader;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class MainMenu implements Screen{

	private Game game;
	private Stage stage;
	
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Sprite background;
	
	private Table table;
	private TextButton newGame;
	private TextButton continueGame;
	private TextButton exitGame;
	
	private Skin buttonSkin;
	
	public MainMenu(Game g){
		
		game = g;
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		batch.setProjectionMatrix(camera.combined);
		
		
		
		batch.disableBlending();
		
		batch.begin();
		
		background.draw(batch);
		
		batch.end();
		
		
		stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();

        //Table.drawDebug(stage); // This is optional, but enables debug lines for tables.
		
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
		camera = new OrthographicCamera();
		camera.viewportHeight = 320;  
        camera.viewportWidth = 480;
        
		camera.position.set(camera.viewportWidth * .5f, camera.viewportHeight * .5f, 0f);  
        camera.update();
        
        //Setup font
        MyGdxGame.font.scale(-0.65f);
        MyGdxGame.font.setColor(Color.WHITE);
        
        batch = new SpriteBatch();
        stage = new Stage(480, 320, true);
        Gdx.input.setInputProcessor(stage);
        
        table = new Table();
        table.setFillParent(true);
        stage.addActor(table);
        
        //TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("data/atlas/atlas_button.txt"));
        //buttonSkin = new Skin(atlas);
        
        //TextureRegion tr = atlas.findRegion("button_up");
        generateButtons();
        
        TextureRegion texRegion;
        
        if(MyGdxGame.resX > 800)
        	texRegion = new TextureRegion( (Texture)MyGdxGame.manager.get(MyGdxGame.MENU), 0, 0, 1600, 1067);
        else
        	texRegion = new TextureRegion( (Texture)MyGdxGame.manager.get(MyGdxGame.MENU), 0, 0, 798, 534);
        
        background = new Sprite(texRegion);
        background.setSize(480, 320);
        background.setOrigin(background.getWidth() / 2, background.getHeight() / 2);
        background.setPosition(0, 0);
        
        
        
	}
	
	private void generateButtons(){
		
        TextureRegion buttonUpReg = new TextureRegion( (Texture)MyGdxGame.manager.get(MyGdxGame.BUTTON_UP) );
        TextureRegion buttonDownReg = new TextureRegion( (Texture)MyGdxGame.manager.get(MyGdxGame.BUTTON_DOWN) );
        
        TextButtonStyle style = new TextButtonStyle();
        style.up = new TextureRegionDrawable(buttonUpReg);
        style.down = new TextureRegionDrawable(buttonDownReg);
        style.font = MyGdxGame.font;
        
        newGame = new TextButton("New Game", style);
        continueGame = new TextButton("Continue", style);
        exitGame = new TextButton("Exit", style);
        
        newGame.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
            	
                return true;  // must return true for touchUp event to occur
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                
            	Storage.deleteSaveFile();
            	game.setScreen(new MyGame(game, 1));
            }
        });
        
        continueGame.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
            	
                return true;  // must return true for touchUp event to occur
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                
            	game.setScreen(new MissionScreen(game));
            }
        });
        
        table.setPosition(table.getX() - 50, table.getY());
        
        if( Storage.generateList().size() > 0 ){
        	
        	table.add(continueGame);
            table.row();
        }
        
        table.add(newGame);
        table.row();
        table.add(exitGame);
        
        table.debug();
        table.debugTable();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		//dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		batch.dispose();
		stage.dispose();
	}
	
	
}
