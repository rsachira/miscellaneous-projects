package com.me.mygdxgame;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;


public class FlyingTile extends GroundTile{
	
	private static final float VELOCITY_CONSTANT = 1000;
	
	private Vector2 velocity;	//in the start to end direction
	private Vector2 start;
	private Vector2 end;
	
	public FlyingTile(World world, float startX, float startY, float width, float endX, float endY, boolean reverse) {
		
		super(world, startX, startY, width, BodyType.KinematicBody);
		
		start = new Vector2(startX, startY);
		end = new Vector2(endX, endY);
		
		if(reverse){
			
			
			tile.tileBody.setTransform(end.x + width / 2, end.y - height / 2, 0);
		}
		
		velocity = Box2DHelper.unitVector( (end.x - start.x), (end.y - start.y) );
		velocity.set(velocity.x * VELOCITY_CONSTANT, velocity.y * VELOCITY_CONSTANT);
	}
	
	public void update(){
		
		
		Vector2 pos = tile.tileBody.getPosition();
		pos.x -= width / 2;
		pos.y += height / 2;
		
		
		if( pos.x >= end.x && pos.y >= end.y && start.y < end.y){
			
			tile.tileBody.setLinearVelocity(-velocity.x, -velocity.y);
		}
		
		if( pos.x >= end.x && pos.y <= end.y && start.y > end.y){
			
			tile.tileBody.setLinearVelocity(-velocity.x, -velocity.y);
		}
		
		if( pos.x <= start.x && pos.y <= start.y && start.y < end.y){
			
			tile.tileBody.setLinearVelocity(velocity.x, velocity.y);
		}
		
		if( pos.x <= start.x && pos.y >= start.y && start.y > end.y){
			
			tile.tileBody.setLinearVelocity(velocity.x, velocity.y);
		}
		
		if( pos.x <= start.x && start.y == end.y){
			
			tile.tileBody.setLinearVelocity(velocity.x, velocity.y);
		}
		
		if( pos.x >= end.x && start.y == end.y){
			
			tile.tileBody.setLinearVelocity(-velocity.x, -velocity.y);
		}
		
		for(int i = 0; i < spriteCollection.size(); i++){
			
			Sprite tileSprite = spriteCollection.get(i);
			tileSprite.setPosition(pos.x + (i * singleWidth), pos.y - tileSprite.getHeight());
		}
		
	}
	
	
}
