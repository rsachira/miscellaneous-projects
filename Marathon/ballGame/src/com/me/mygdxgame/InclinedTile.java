package com.me.mygdxgame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

public class InclinedTile {
	
	private Tile tile;
	private static float angle;
	private static final float height = 25.0f;
	private static final float singleWidth = 25.0f;
	
	private ArrayList<Sprite> spriteCol = new ArrayList<Sprite>();
	private float indexWidth;
	
	private float fX = 0.0f;
	private float fY = 0.0f;
	
	private Texture groundTex;
	
	private float realWidth(float width){
		
		return width;// / (float)Math.cos(angle);
	}
	
	public InclinedTile(World world, float X, float Y, float width, char type, BodyType bodyType){
		
		this.indexWidth = (width / singleWidth);
		
		float tileX = X;
		float tileY = Y;
		
		groundTex = (Texture)MyGdxGame.manager.get(MyGdxGame.GROUND);
		groundTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		float angleInDeg = 0;
		
		if(type == 'r'){
			
			angleInDeg = angle = 45.0f;
			//tileY += (width / 2) * (float)Math.sin(angle);
			
		}else{
			
			angleInDeg = angle = -45.0f;
			//tileY -= (width / 2) * (float)Math.sin(angle);
			
		}
		
		angle = (float)Math.PI / 180 * angle;
		
		fX = tileX;
		fY = tileY;
		
		tileX += (width / 2) * (float)Math.cos(angle);
		//tileX += (height / 2) * Math.sin(angle);
		//tileY += 2;
		tileY += (width / 2) * (float)Math.sin(angle);
		
		tile = new Tile(world, realWidth(width), height, tileX, tileY, angle, bodyType);
		generateSprite(realWidth(width), height, tileX - (width / 2), tileY - (height / 2), angleInDeg);
	}
	
	private void generateSprite(float width, float height, float tileX, float tileY, float angle){
		
		float angleInRad = (float)Math.PI / 180 * angle;
		
		fX += (height / 2) * (float)Math.sin(angleInRad);
		fY -= (height / 2) * (float)Math.cos(angleInRad);
		
		for(int i = 0; i < indexWidth; i++){
			
			Sprite tileSprite = new Sprite(groundTex);
			tileSprite.setSize(singleWidth, height);
			tileSprite.setOrigin(0, 0);
			tileSprite.setPosition(fX, fY);
			tileSprite.setRotation(angle);
			
			spriteCol.add(tileSprite);
			
			fX += singleWidth * Math.cos(angleInRad);
			fY += singleWidth * Math.sin(angleInRad);
		}
		
		SpriteWrapper sw = new SpriteWrapper();
		sw.spriteCollection = spriteCol;
		sw.shouldDraw = true;
		
		MyGame.sprites.add(sw);
	}
	
}
