package com.me.mygdxgame;

public class UserData {
	
	enum Type{
		
		BALL, COIN, TILE, BUTTON
	}
	
	public Type objType;
	public int value;
	public int spriteIndex;
	
	public UserData(Type type, int coinValue, int index){
		
		objType = type;
		spriteIndex = index;
		value = coinValue;
	}
	
}
