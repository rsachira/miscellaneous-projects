package com.me.mygdxgame;

import java.util.ArrayList;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.randezvou.app.controllers.DragController;
import com.randezvou.app.controllers.JoyStick;
import com.randezvou.app.joints.WheelJoint;
import com.randezvou.app.save.SaveFuncs;
import com.randezvou.app.save.State;

public class MyGame implements Screen, InputProcessor, ContactListener {
	
	
	World world;
	Box2DDebugRenderer debugRenderer;
	
	static final float WORLD_TO_BOX = 0.01f;
	static final float BOX_WORLD_TO = 100f;
	static final float BOX_STEP = 1/60f;
    static final int BOX_VELOCITY_ITERATIONS = 6;
    static final int BOX_POSITION_ITERATIONS = 2;
    static final boolean FOLLOW = true;
    static final float SCREEN_HEIGHT = 768;
    
    public static Ball ball;
    public static Ball balloony;
	
	public static OrthographicCamera camera;
	
	private SpriteBatch batch;
	public static ArrayList<SpriteWrapper> sprites = new ArrayList<SpriteWrapper>();
	public static ArrayList<FlyingTile> flyingTiles = new ArrayList<FlyingTile>();
	public static ArrayList<OrdinaryCoin> coins = new ArrayList<OrdinaryCoin>();
	public static ArrayList<Sprite[]> grassSprites = new ArrayList<Sprite[]>();
	public static ArrayList<JoyStick> joysticks = new ArrayList<JoyStick>();
	
	public static DragController dragController = new DragController();
	
	//public static Texture groundTileTex;
	//public static Texture coinTex;
	
	private Vector2 fingerInitial;
	private MapBuilder maps;
	
	private boolean timer_start;
	private float elapsed_time;
	private float time;
	
	private Sound boing;
	
	enum GameState{
		
		WON, LOST, RUNNING, MENU
	}
	
	GameState state = GameState.MENU;
	
	public static ArrayList<Body> deleteList = new ArrayList<Body>();
	
	Background bg1;
	Background mountainBack;
	Background mountainTop;
	
	private int levelNo;
	public static int totalCoins = 0;
	public static float lastTouchDown = 0;
	
	private Game game;

	public MyGame(Game g, int levelNo){
		
		game = g;
		this.levelNo = levelNo;
		state = GameState.RUNNING;
	}
	
	//Grass grass;
	ShapeRenderer sr = new ShapeRenderer();
	boolean drawShape = false;
	float X1, Y1, X2, Y2 = 0.0f;
	float preCameraX = 0;
	
	//Games Current Intel. Used to pause and resume
	State currentState = new State();
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		
		if(state == GameState.MENU){
			
			//batch.setProjectionMatrix(camera.combined);
			//menuScreen.render(batch);
			//return;
		}
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		
        world.step(BOX_STEP, BOX_VELOCITY_ITERATIONS, BOX_POSITION_ITERATIONS);
        
        for(int i=0; i < deleteList.size(); i++){
        	
        	Body body = deleteList.get(i);
        	
        	if(body == null)
        		continue;
        	
        	world.destroyBody(body);
        	deleteList.remove(i);
        }
        
        if(FOLLOW && shouldFollow(ball.ballBody.getLinearVelocity(), ball.ballBody.getPosition(), camera.position.x) ){
        	camera.position.set( ball.ballBody.getPosition().x, camera.viewportHeight * .5f, 0f);
        	camera.update();
        }
        
        //Move background to create a slower background moving speed(relative to the camera)
        float deltaCameraX = camera.position.x - preCameraX;
        bg1.move(deltaCameraX / 30);
        mountainBack.move(deltaCameraX / 20);
        mountainTop.move(deltaCameraX / 10);
        preCameraX = camera.position.x;
        
        //check whether lost
        if(ball.ballBody.getPosition().y < -ball.width)
        	state = GameState.LOST;
        
        if(state == GameState.LOST){
        	
        	// if lost restart the game
        	restart();
        }
        
        if(state == GameState.WON){
        	
        	game.setScreen(new WonScreen(game, ball.getScore() + getTimeBonus(time), MyGame.totalCoins, levelNo));
        }
        
        batch.setProjectionMatrix(camera.combined);
        
        batch.enableBlending();
        batch.begin();
        
        bg1.update(camera.position.x - (camera.viewportWidth / 2) );
        mountainBack.update(camera.position.x - (camera.viewportWidth / 2) );
        mountainTop.update(camera.position.x - (camera.viewportWidth / 2) );
        //bg2.update(camera.position.x - (camera.viewportWidth / 2) );
        bg1.getBackSprite().draw(batch);
        bg1.getNextBack().draw(batch);
        mountainBack.getBackSprite().draw(batch);
        mountainBack.getNextBack().draw(batch);
        mountainTop.getBackSprite().draw(batch);
        mountainTop.getNextBack().draw(batch);
        
        MyGdxGame.tweenManager.update(Gdx.graphics.getDeltaTime());
        
        // Draw Grass
        for(int i = 0; i < grassSprites.size(); i++)
        	for(int j = 0; j < 3; j++)
        		grassSprites.get(i)[j].draw(batch);
        
        batch.end();
        
        ball.ballSprite.setPosition(ball.ballBody.getPosition().x - (ball.width / 2), ball.ballBody.getPosition().y - (ball.width / 2) );
        balloony.ballSprite.setPosition(balloony.ballBody.getPosition().x - (balloony.width / 2), balloony.ballBody.getPosition().y - (balloony.width / 2) );
        
        //Set Horizontal Linear Velocity to the ball
        //ball.ballBody.setLinearVelocity(100f, 0);
        
		batch.enableBlending();
        batch.begin();
        
        //ball.ballSprite.draw(batch);
        ball.blink();
        
        for(int i=0; i < sprites.size(); i++)
        	if(sprites.get(i).shouldDraw){
        		
        		ArrayList<Sprite> collection = sprites.get(i).spriteCollection;
        		
        		for(int j = 0; j < collection.size(); j++)
        			collection.get(j).draw(batch);
        	}
        
        
        
        //fl.update();
        //grass.update();
        //grass.draw(batch);
        
        for(int i = 0; i < flyingTiles.size(); i++)
        	flyingTiles.get(i).update();
        
        //Update Cloud
        WindCloud.lCloud.update();
        WindCloud.rCloud.update();
        WindCloud.tCloud.update();
        WindCloud.bCloud.update();
        
        //Draw cloud
        WindCloud.lCloud.cloudSprite.draw(batch);
        WindCloud.rCloud.cloudSprite.draw(batch);
        WindCloud.tCloud.cloudSprite.draw(batch);
        WindCloud.bCloud.cloudSprite.draw(batch);
        
        drawScore(ball.getScore());
        
        //Draw elapsed time
        if(timer_start)
        	time += Gdx.graphics.getDeltaTime();
        
        drawTime(time);
        
        /*for(int i = 0; i < joysticks.size(); i++){
        	
        	JoyStick j = joysticks.get(i);
        	j.update();
        	
        	if(j.sw.shouldDraw)
        		for(int k = 0; k < j.sw.spriteCollection.size(); k++)
        			j.sw.spriteCollection.get(k).draw(batch);
        }*/
        
        batch.end();
        
        if(drawShape){
        	
        	sr.setProjectionMatrix(camera.combined);
        	sr.begin(ShapeType.Line);
        	sr.setColor(0, 0, 0, 0);
        	sr.line(X1, Y1, X2, Y2);
        	sr.end();
        }
        dragController.draw(sr, camera);
        
        generateForce();	// Testing
        debugRenderer.render(world, camera.combined);
	}
	
	private void restart(){	// restart the game
		
		dispose();
    	
    	state = GameState.RUNNING;
    	show();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}
	
	FlyingTile fl;
	RayCastCallback  intersection;
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
		// Setup world
		world = new World(new Vector2(0, -100), true);
		world.setContactListener(this);
		
		// Setup camera
		camera = new OrthographicCamera();
		camera.viewportHeight = 320;
        camera.viewportWidth = 480;
        camera.position.set(camera.viewportWidth * .5f, camera.viewportHeight * .5f, 0f);
        camera.update();
        
        // Initialize variables
        sprites = new ArrayList<SpriteWrapper>();
        flyingTiles = new ArrayList<FlyingTile>();
        //joysticks = new ArrayList<JoyStick>();
        MyGame.totalCoins = 0;
        TweenCallBacks.coinsBouncing = false;
        timer_start = false;
        time = 0;
        elapsed_time = 0;
        preCameraX = camera.position.x;
        
        Tween.registerAccessor(OrdinaryCoin.class, new CoinAccessor());
        Tween.registerAccessor(Sprite.class, new GrassSpriteAccessor());
        Tween.registerAccessor(Cloud.class, new CloudAccessor());
        
        batch = new SpriteBatch();
		debugRenderer = new Box2DDebugRenderer();
		
		// Joystick test
		//JoyStick js = new JoyStick(400, 60, (Texture)MyGdxGame.manager.get(MyGdxGame.JOY_HANDLE), (Texture)MyGdxGame.manager.get(MyGdxGame.JOY_STICK), world);
		//joysticks.add(js);
		
		/* Load Textures
		
		groundTileTex = new Texture(Gdx.files.internal("data/ground.png"));
		groundTileTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		coinTex = new Texture(Gdx.files.internal("data/Coin.png"));
		coinTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
        
        Texture backgroundTex = new Texture(Gdx.files.internal("data/background1.png"));*/
		
		//Car Structure
		Body carBox = Box2DHelper.createBody(world, new Vector2(100, 300), 0, BodyType.DynamicBody);
		Shape carBoxShape = Box2DHelper.box(30, 20);
		Fixture carBoxFixture = Box2DHelper.createFixture(carBox, carBoxShape, 1.0f, 1.0f, 0.0f);
		
		Body wheel1 = Box2DHelper.createBody(world, new Vector2(300, 300), 0, BodyType.DynamicBody);
		Shape wheel1Shape = Box2DHelper.circle(10);
		Fixture wheel1Fixture = Box2DHelper.createFixture(wheel1, wheel1Shape, 1.0f, 1.0f, 0.0f);
		
		Body wheel2 = Box2DHelper.createBody(world, new Vector2(100, 300), 0, BodyType.DynamicBody);
		Shape wheel2Shape = Box2DHelper.circle(10);
		Fixture wheel2Fixture = Box2DHelper.createFixture(wheel2, wheel2Shape, 1.0f, 1.0f, 0.0f);
		
		WheelJoint j1 = new WheelJoint(carBox, wheel1, false);
		j1.setAnchorA(-15, -20);
		j1.setAxis(Vector2.Y);
		j1.createJoint(world);
		
		WheelJoint j2 = new WheelJoint(carBox, wheel2, false);
		j2.setAnchorA(15, -20);
		j2.setAxis(Vector2.Y);
		j2.createJoint(world);
		
		TextureRegion backTexRegion = new TextureRegion( (Texture)MyGdxGame.manager.get(MyGdxGame.BACKGROUND_1), 2048, 800);
		TextureRegion mountainBackTexRegion = new TextureRegion( (Texture)MyGdxGame.manager.get(MyGdxGame.MOUNTAIN_BACK), 2048, 670);
		TextureRegion mountainTopTexRegion = new TextureRegion( (Texture)MyGdxGame.manager.get(MyGdxGame.MOUNTAIN_TOP), 2048, 677);
		
        bg1 = new Background(0, 0, 819.2f, 320, backTexRegion);
        mountainBack = new Background(0, 0, 768f, 300, mountainBackTexRegion);
        mountainTop = new Background(0, 0, 768f, 300, mountainTopTexRegion);
        
        boing = Gdx.audio.newSound(Gdx.files.internal("data/sounds/Boing.mp3"));
        
        MyGdxGame.font.setScale(1, 1);
        MyGdxGame.font.scale(-0.75f);
        MyGdxGame.font.setColor(Color.BLACK);
        
        //fl = new FlyingTile(world, 5, 8, 2, 8, 8, true);
        
        // Testing
        //grass = new Grass(0, 0);
        
        if(!Gdx.files.internal("data/levels/" + Integer.toString(levelNo) + ".txt").exists()){
        	
        	game.setScreen(new MissionScreen(game));
        	return;
        }
        
        /*maps = new Map(world, game);
        maps.loadMap("data/levels/" + Integer.toString(levelNo) + ".txt");
        totalCoins = maps.getTotalCoins();*/
        
        WindCloud.initialize();
        
        maps = new MapBuilder(world);
        
        if( currentState.generateState() ){
        	
        	try{
        		maps.loadMap("data/levels/" + Integer.toString(this.levelNo) + ".txt", currentState.getCoinsAvailable());
        		
        		ball.setScore( currentState.getScore() );
        		this.time = currentState.getTime();
        		ball.ballBody.setTransform(currentState.getLastTouchDown().x, currentState.getLastTouchDown().y, 0);
        		
        		SaveFuncs.setPause(false);
        	}finally{ SaveFuncs.setPause(false); }
        	
        }else
        	maps.loadMap("data/levels/" + Integer.toString(this.levelNo) + ".txt");
        //Tile ground = new Tile(world, 100.0f, 20.0f, (camera.viewportWidth / 2), 10.0f, 10);
        //ball = new Ball(world, 18.0f, new Vector2( (camera.viewportWidth / 2), (camera.viewportHeight / 2) ) );
        
		Gdx.input.setInputProcessor(this);
	}
	
	private void drawScore(int score){
		
		String strScore = Integer.toString(score);
		
		float textWidth = MyGdxGame.font.getBounds(strScore).width;
		
		float x = camera.position.x + (camera.viewportWidth / 2) - textWidth - 5;
		float y = camera.position.y + (camera.viewportHeight / 2) + 10;
		
		MyGdxGame.font.draw(batch, strScore, x, y);
		
	}
	
	private void drawTime(float time){
		
		String strTime = Float.toString(time);
		
		float x = camera.position.x - (camera.viewportWidth / 2) + 5;
		float y = camera.position.y + (camera.viewportHeight / 2) + 10;
		
		MyGdxGame.font.draw(batch, strTime, x, y);
	}
	
	private int getTimeBonus(float timeElapsed){
		
		double bonus = 0;
		bonus = Math.pow(1.02, -1 * timeElapsed);
		bonus *= 100;
		
		return (int)bonus;
	}
	
	/*public float gameX(float screenX){
		
		float center_theoretical = MyGdxGame.resX / 2;
		float center_practical = camera.position.x;
		
		return screenX * (center_practical / center_theoretical);
	}*/
	
	private boolean shouldFollow(Vector2 ballVelocity, Vector2 ballPos, float cameraX){
		
		if( ((ballVelocity.x > 0) && (ballPos.x >= cameraX)) || ((ballVelocity.x < 0) && (ballPos.x <= cameraX) && (camera.position.x > (camera.viewportWidth / 2) ) ) )
				return true;
		
		return false;
	}
	
	public float gameX(float screenX){
		
		float halfWidth = 240;
		
		float center_theoretical = MyGdxGame.resX / 2;
		float center_practical = camera.position.x;
		
		return ( screenX * (center_practical / center_theoretical) ) + (camera.position.x - halfWidth);
		
		
		//return (2 * camera.position.x) / 480 * screenX;
		
	}
	
	public float gameY(float screenY){
		
		float center_theoretical = MyGdxGame.resY / 2;
		float center_practical = camera.position.y;
		
		return screenY * (center_practical / center_theoretical);
	}
	
	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
		dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		currentState.setCoinsAvailable(coins);
		currentState.setTime(time);
		currentState.setScore( ball.getScore() );
		currentState.saveState();
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		restart();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
		batch.dispose();
		boing.dispose();
		sprites = new ArrayList<SpriteWrapper>();
		deleteList = new ArrayList<Body>();
		flyingTiles = new ArrayList<FlyingTile>();
		coins = new ArrayList<OrdinaryCoin>();
		grassSprites = new ArrayList<Sprite[]>();
		//joysticks = new ArrayList<JoyStick>();
		MyGdxGame.tweenManager.killAll();
		MyGdxGame.tweenManager = new TweenManager();
		TweenCallBacks.coinsBouncing = false;
		TweenCallBacks.grassShaking = false;
		//world.dispose();
	}

	@Override
	public void beginContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
		Fixture A = contact.getFixtureA();
		Fixture B = contact.getFixtureB();
		
		if(A.getUserData() != null && B.getUserData() != null){
			
			UserData userA = (UserData)A.getUserData();
			UserData userB = (UserData)B.getUserData();
			
			if(userA.objType == UserData.Type.BALL && userB.objType == UserData.Type.CONTROLLER){
				
				
				contact.setEnabled(false);
				return;
			}
			
			if(userB.objType == UserData.Type.BALL && userA.objType == UserData.Type.CONTROLLER){
				
				
				contact.setEnabled(false);
				return;
			}
			
			if(userA.objType == UserData.Type.BALL && userB.objType == UserData.Type.COIN){
				
				
				if(sprites.get(userB.spriteIndex).shouldDraw == false){
					
					contact.setEnabled(false);
					return;
				}
				
				
				ball.setScore(ball.getScore() + userB.value);
				OrdinaryCoin.shouldRemove(userB.spriteIndex);
				//removeAnimation(userB.spriteIndex);
				//deleteList.add(B.getBody());
				contact.setEnabled(false);
				
				sprites.get(userB.spriteIndex).shouldDraw = false;
				
				return;
			}
			
			if(userB.objType == UserData.Type.BALL && userA.objType == UserData.Type.COIN){
				
				
				if(sprites.get(userA.spriteIndex).shouldDraw == false){
					
					contact.setEnabled(false);
					return;
				}
				
				ball.setScore(ball.getScore() + userA.value);
				OrdinaryCoin.shouldRemove(userA.spriteIndex);
				//removeAnimation(userA.spriteIndex);
				//deleteList.add(A.getBody());
				contact.setEnabled(false);
				
				sprites.get(userA.spriteIndex).shouldDraw = false;
				
				return;
			}
			
			if(userA.objType == UserData.Type.BALL && userB.objType == UserData.Type.BALL){
				
				timer_start = false;
				state = GameState.WON;
			}
			
			if( (userA.objType == UserData.Type.BALL && userB.objType == UserData.Type.TILE && withinCurrentFrame(A.getBody().getPosition()) ) || (userA.objType == UserData.Type.TILE && userB.objType == UserData.Type.BALL && withinCurrentFrame(B.getBody().getPosition())) ){
				
				currentState.setLastTouchDown( ball.ballBody.getPosition() );
				boing.play();
			}
			
		}
		
	}
	
	public boolean withinCurrentFrame(Vector2 pos){
		
		if( (camera.position.x + (camera.viewportWidth / 2) ) >= pos.x && (camera.position.x - (camera.viewportWidth / 2) ) <= pos.x )
			if( (camera.position.y + (camera.viewportHeight / 2) ) >= pos.y && (camera.position.y - (camera.viewportHeight / 2) ) <= pos.y )
				
				return true;
		
		return false;
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		
		if(!timer_start && state == GameState.RUNNING)
			timer_start = true;
		
		dragController.onTouchDown(camera, screenX, screenY);
		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		
		WindCloud.hideLeft();
		WindCloud.hideRight();
		WindCloud.hideTop();
		WindCloud.hideBottom();
		
		/*for(int i = 0; i < joysticks.size(); i++)
			joysticks.get(i).touched = false;*/
		
		dragController.onTouchUp();
		
		return false;
	}
	
	Vector3 screenCoords = new Vector3();
	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		
		//float X = gameX(screenX);
		//float Y = gameY(MyGdxGame.resY - screenY);
		
		screenCoords.x = screenX;
		screenCoords.y = screenY;
		screenCoords.z = 0;
		
		camera.unproject(screenCoords);
		float X = screenCoords.x;
		float Y = screenCoords.y;
		
		/*for(int i = 0; i < joysticks.size(); i++)
			joysticks.get(i).onMove( new Vector2(X, Y) );*/
		
		dragController.onTouchDragged(camera, screenX, screenY);
		
		return false;
	}
	
	private void generateForce(){
		
		final float MULTIPLIER = 1000;
		
		/*if(joysticks.size() == 0)
			return;
		
		Vector2 data = joysticks.get(0).getData();
		
		Vector2 force = new Vector2();
		force.x = MULTIPLIER * data.x * (float)Math.sin(data.y);
		force.y = MULTIPLIER * data.x * (float)Math.cos(data.y);
		
		force = Box2DHelper.unitVector(joysticks.get(0).deltaX, joysticks.get(0).deltaY);
		force.x *= MULTIPLIER * data.x;
		force.y *= MULTIPLIER * data.x;*/
		
		Vector2 force = dragController.getForce(MULTIPLIER);
		
		showClouds(force);
		
		ball.ballBody.applyLinearImpulse(force, ball.ballBody.getWorldCenter(), true);
	}
	
	
	private void showClouds(Vector2 force){
		
		if(force.x > 100)
			WindCloud.showLeft();
		else
			WindCloud.hideLeft();
		
		if(force.x < -100)
			WindCloud.showRight();
		else
			WindCloud.hideRight();
		
		if(force.y < -100)
			WindCloud.showTop();
		else
			WindCloud.hideTop();
		
		if(force.y > 100)
			WindCloud.showBottom();
		else
			WindCloud.hideBottom();
		
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		
		//System.out.println( gameX(screenX) );
		
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
