package com.me.mygdxgame;

import java.util.ArrayList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.physics.box2d.World;

public class MyGame implements Screen, InputProcessor, ContactListener {
	
	
	World world;
	Box2DDebugRenderer debugRenderer;
	
	static final float WORLD_TO_BOX = 0.01f;
	static final float BOX_WORLD_TO = 100f;
	static final float BOX_STEP = 1/60f;
    static final int BOX_VELOCITY_ITERATIONS = 6;
    static final int BOX_POSITION_ITERATIONS = 2;
    static final boolean FOLLOW = true;
    static final float SCREEN_HEIGHT = 768;
    
    Ball ball;
	
	private OrthographicCamera camera;
	
	private SpriteBatch batch;
	public static ArrayList<SpriteWrapper> sprites = new ArrayList<SpriteWrapper>();
	public static ArrayList<FlyingTile> flyingTiles = new ArrayList<FlyingTile>();
	
	private Vector2 fingerInitial;
	private IO maps;
	
	private Sound boing;
	
	private float resX, resY = 0;
	
	enum GameState{
		
		WON, LOST, RUNNING, MENU
	}
	
	GameState state = GameState.MENU;
	
	private static ArrayList<Body> deleteList = new ArrayList<Body>();
	
	Background bg1;
	Background bg2;
	
	BitmapFont font;
	
	private int levelNo;
	private int totalCoins;
	
	private Game game;

	public MyGame(Game g, int levelNo){
		
		game = g;
		this.levelNo = levelNo;
	}
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		
		if(state == GameState.MENU){
			
			//batch.setProjectionMatrix(camera.combined);
			//menuScreen.render(batch);
			//return;
		}
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		
        world.step(BOX_STEP, BOX_VELOCITY_ITERATIONS, BOX_POSITION_ITERATIONS);
        
        for(int i=0; i < deleteList.size(); i++){
        	
        	Body body = deleteList.get(i);
        	
        	if(body == null)
        		continue;
        	
        	world.destroyBody(body);
        	deleteList.remove(i);
        	
        }
        
        if(FOLLOW){
        	camera.position.set( ball.ballBody.getPosition().x, camera.viewportHeight * .5f, 0f);
        	camera.update();
        }
        
        //check whether lost
        if(ball.ballBody.getPosition().y < -ball.width)
        	state = GameState.LOST;
        
        if(state == GameState.LOST){
        	
        	// if lost restart the game
        	restart();
        }
        
        if(state == GameState.WON){
        	
        	game.setScreen(new WonScreen(game, ball.getScore(), totalCoins, levelNo));
        }
        
        batch.setProjectionMatrix(camera.combined);
        
        batch.enableBlending();
        batch.begin();
        
        bg1.update(camera.position.x - (camera.viewportWidth / 2) );
        //bg2.update(camera.position.x - (camera.viewportWidth / 2) );
        bg1.getBackSprite().draw(batch);
        //bg2.getBackSprite().draw(batch);
        bg1.getNextBack().draw(batch);
        
        batch.end();
        
        ball.ballSprite.setPosition(ball.ballBody.getPosition().x - (ball.width / 2), ball.ballBody.getPosition().y - (ball.width / 2) );
        maps.balloony.ballSprite.setPosition(maps.balloony.ballBody.getPosition().x - (maps.balloony.width / 2), maps.balloony.ballBody.getPosition().y - (maps.balloony.width / 2) );
        
		batch.enableBlending();
        batch.begin();
        
        //ball.ballSprite.draw(batch);
        ball.blink();
        
        for(int i=0; i < sprites.size(); i++)
        	if(sprites.get(i).shouldDraw)
        		sprites.get(i).sprite.draw(batch);
        
        drawScore(ball.getScore());
        
        //fl.update();
        
        for(int i = 0; i < flyingTiles.size(); i++)
        	flyingTiles.get(i).update();
        
        batch.end();
        
        debugRenderer.render(world, camera.combined);
	}
	
	private void restart(){	// restart the game
		
		dispose();
    	world.dispose();
    	deleteList = new ArrayList<Body>();
    	sprites = new ArrayList<SpriteWrapper>();
    	
    	state = GameState.RUNNING;
    	show();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}
	
	FlyingTile fl;
	
	@Override
	public void show() {
		// TODO Auto-generated method stub
		
		world = new World(new Vector2(0, -100), true);
		world.setContactListener(this);
		
		camera = new OrthographicCamera();
		camera.viewportHeight = 320;  
        camera.viewportWidth = 480;
        
		camera.position.set(camera.viewportWidth * .5f, camera.viewportHeight * .5f, 0f);  
        camera.update();
        
        resX = 1280;
        resY = 768;
        
        batch = new SpriteBatch();
		debugRenderer = new Box2DDebugRenderer();
        
        Texture backgroundTex = new Texture(Gdx.files.internal("data/background1.png"));
        bg1 = new Background(0, 0, 1024, 1024, backgroundTex);
        bg2 = new Background(1024, 0, 1024, 1024, backgroundTex);
        
        boing = Gdx.audio.newSound(Gdx.files.internal("data/sounds/clink.wav"));
        
        font = new BitmapFont();
        
        //fl = new FlyingTile(world, 5, 8, 2, 8, 8, true);
        
        if(!Gdx.files.internal("data/levels/" + Integer.toString(levelNo) + ".txt").exists()){
        	
        	game.setScreen(new MissionScreen(game));
        	return;
        }
        
        maps = new IO(world, game);
        maps.loadMap("data/levels/" + Integer.toString(levelNo) + ".txt");
        totalCoins = maps.getTotalCoins();
        
        //Tile ground = new Tile(world, 100.0f, 20.0f, (camera.viewportWidth / 2), 10.0f, 10);
        ball = new Ball(world, 18.0f, new Vector2( (camera.viewportWidth / 2), (camera.viewportHeight / 2) ) );
		
		Gdx.input.setInputProcessor(this);
	}
	
	private void drawScore(int score){
		
		String strScore = Integer.toString(score);
		
		float textWidth = font.getBounds(strScore).width;
		
		float x = camera.position.x + (camera.viewportWidth / 2) - textWidth - 3;
		float y = camera.position.y + (camera.viewportHeight / 2) - 3;
		
		font.draw(batch, strScore, x, y);
		
	}
	
	public float gameX(float screenX){
		
		float center_theoretical = resX / 2;
		float center_practical = camera.position.x;
		
		return screenX * (center_practical / center_theoretical);
	}
	
	public float gameY(float screenY){
		
		float center_theoretical = resY / 2;
		float center_practical = camera.position.y;
		
		return screenY * (center_practical / center_theoretical);
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		batch.dispose();
		boing.dispose();
		world.dispose();
	}

	@Override
	public void beginContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void endContact(Contact contact) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
		// TODO Auto-generated method stub
		
		Fixture A = contact.getFixtureA();
		Fixture B = contact.getFixtureB();
		
		if(A.getUserData() != null && B.getUserData() != null){
			
			UserData userA = (UserData)A.getUserData();
			UserData userB = (UserData)B.getUserData();
			
			if(userA.objType == UserData.Type.BALL && userB.objType == UserData.Type.COIN){
				
				ball.setScore(ball.getScore() + userB.value);
				deleteList.add(B.getBody());
				contact.setEnabled(false);
				
				sprites.get(userB.spriteIndex).shouldDraw = false;
				
				return;
			}
			
			if(userB.objType == UserData.Type.BALL && userA.objType == UserData.Type.COIN){
				
				ball.setScore(ball.getScore() + userA.value);
				deleteList.add(A.getBody());
				contact.setEnabled(false);
				
				sprites.get(userA.spriteIndex).shouldDraw = false;
				
				return;
			}
			
			if(userA.objType == UserData.Type.BALL && userB.objType == UserData.Type.BALL){
				
				state = GameState.WON;
			}
			
			if( (userA.objType == UserData.Type.BALL && userB.objType == UserData.Type.TILE && withinCurrentFrame(A.getBody().getPosition()) ) || (userA.objType == UserData.Type.TILE && userB.objType == UserData.Type.BALL && withinCurrentFrame(B.getBody().getPosition())) ){
				
				boing.play();
			}
			
		}
		
	}
	
	public boolean withinCurrentFrame(Vector2 pos){
		
		if( (camera.position.x + (camera.viewportWidth / 2) ) >= pos.x && (camera.position.x - (camera.viewportWidth / 2) ) <= pos.x )
			if( (camera.position.y + (camera.viewportHeight / 2) ) >= pos.y && (camera.position.y - (camera.viewportHeight / 2) ) <= pos.y )
				
				return true;
		
		return false;
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean keyDown(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		
		float X = gameX(screenX);
		float Y = gameY(SCREEN_HEIGHT - screenY);
		
		fingerInitial = new Vector2(X, Y);
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		
		float X = gameX(screenX);
		float Y = gameY(SCREEN_HEIGHT - screenY);
		
		if( Box2DHelper.modulus(ball.ballBody.getPosition(), new Vector2(X, Y)) > 30 )
			return false;
		
		float deltaX = X - fingerInitial.x;
		float deltaY = Y - fingerInitial.y;
		
		Vector2 force = Box2DHelper.unitVector(deltaX, deltaY);
		force.x *= 8000;
		force.y *= 8000;
		
		ball.ballBody.applyLinearImpulse(force, new Vector2(X, Y), true);
		
		System.out.println("Velocity in X direction = " + ball.ballBody.getLinearVelocity().x);
		System.out.println("Velocity in Y direction = " + ball.ballBody.getLinearVelocity().y);
		
		fingerInitial.x = X;
		fingerInitial.y = Y;
		
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
