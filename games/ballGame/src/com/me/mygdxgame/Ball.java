package com.me.mygdxgame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;

public class Ball {
	
	private static final int NUM_CYCLES = 5;
	
	private int life;
	private int score;
	private float startX, startY;
	private float radius;
	
	public Body ballBody;
	private Shape ballShape;
	
	private Texture ballTexNormal;
	private Texture ballTexBlink;
	
	public Sprite ballSprite;
	public float width;
	
	public boolean eyesClosed = false;
	private int curr_cycle = 0;
	
	private ArrayList<Sprite> spriteCol = new ArrayList<Sprite>();
	
	public Ball(World world, float ballRadius, Vector2 pos){
		
		radius = ballRadius;
		startX = pos.x;
		startY = pos.y;
		
		life = 100;
		score = 0;
		
		ballBody = Box2DHelper.createBody(world, pos, 0, BodyType.DynamicBody);
		ballShape = Box2DHelper.circle(radius);
		
		Fixture ballFixture = Box2DHelper.createFixture(ballBody, ballShape, 1.0f, 1.0f, 0.0f);
		
		width = ballRadius * 2;
		int index = generateSprite(width, width, pos.x - (width / 2), pos.y - (width / 2) );
		
		UserData us = new UserData(UserData.Type.BALL, 0, index);
		ballFixture.setUserData(us);
		
	}
	
	private int generateSprite(float width, float height, float ballX, float ballY){
		
		ballTexNormal = new Texture(Gdx.files.internal("data/ball_new.png"));
		ballTexNormal.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		ballTexBlink = new Texture(Gdx.files.internal("data/ball_new_blink.png"));
		ballTexBlink.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		ballSprite = new Sprite(ballTexNormal);
		ballSprite.setSize(width, height);
		ballSprite.setOrigin(ballSprite.getWidth()/2, ballSprite.getHeight()/2);
		ballSprite.setPosition(ballX, ballY - ballSprite.getHeight());
		
		spriteCol.add(ballSprite);
		
		SpriteWrapper sw = new SpriteWrapper();
		sw.spriteCollection = spriteCol;
		sw.shouldDraw = true;
		MyGame.sprites.add(sw);
		
		return MyGame.sprites.size() - 1;
	}
	
	public void blink(){
		
		if(curr_cycle < NUM_CYCLES){
			
			curr_cycle++;
			return;
		}
		
		curr_cycle = 0;
		
		int num = MathUtils.random(1, 30);
		
		if(num == 3 && eyesClosed == false){
			
			ballSprite.setTexture(ballTexBlink);
			eyesClosed = true;
			
		}else{
			
			ballSprite.setTexture(ballTexNormal);
			eyesClosed = false;
		}
	}

	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	
}
