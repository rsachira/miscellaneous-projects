package com.me.mygdxgame;

import aurelienribon.tweenengine.Tween;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class WindCloud {
	
	public static final float HALF_WIDTH = 240;
	public static final float HALF_HEIGHT = 160;
	
	private static Sprite cloud_left;
	private static Sprite cloud_right;
	private static Sprite cloud_top;
	private static Sprite cloud_bottom;
	
	public static final float DELTA_LEFT = 70;
	public static final float DELTA_RIGHT = 70;
	public static final float DELTA_TOP = 60;
	public static final float DELTA_BOTTOM = 60;
	
	public static float leftX = 0;
	public static float rightX = 0;
	public static float topY = 0;
	public static float bottomY = 0;
	
	public static Cloud lCloud;	//Left cloud
	public static Cloud rCloud;	//Right cloud
	public static Cloud tCloud;	//Top cloud
	public static Cloud bCloud;	//Bottom cloud
	
	public static boolean leftCloudInBound = false;
	public static boolean rightCloudInBound = false;
	public static boolean topCloudInBound = false;
	public static boolean bottomCloudInBound = false;
	
	public static void initialize(){
		
		TextureRegion cloud_left_tex = new TextureRegion((Texture)MyGdxGame.manager.get(MyGdxGame.CLOUD_LEFT), 512, 338);
		TextureRegion cloud_right_tex = new TextureRegion((Texture)MyGdxGame.manager.get(MyGdxGame.CLOUD_RIGHT), 512, 338);
		TextureRegion cloud_top_tex = new TextureRegion((Texture)MyGdxGame.manager.get(MyGdxGame.CLOUD_TOP), 512, 338);
		TextureRegion cloud_bottom_tex = new TextureRegion((Texture)MyGdxGame.manager.get(MyGdxGame.CLOUD_BOTTOM), 512, 338);
		
		cloud_left = new Sprite( cloud_left_tex );
		cloud_right = new Sprite( cloud_right_tex );
		cloud_top = new Sprite( cloud_top_tex );
		cloud_bottom = new Sprite( cloud_bottom_tex );
		
		cloud_left.setSize(145.45f, 96);
		cloud_right.setSize(145.45f, 96);
		cloud_top.setSize(145.45f, 96);
		cloud_bottom.setSize(145.45f, 96);
		
		cloud_left.setOrigin(cloud_left.getWidth()/2, cloud_left.getHeight()/2);
		cloud_right.setOrigin(cloud_left.getWidth()/2, cloud_left.getHeight()/2);
		cloud_top.setOrigin(cloud_left.getWidth()/2, cloud_left.getHeight()/2);
		cloud_bottom.setOrigin(cloud_left.getWidth()/2, cloud_left.getHeight()/2);
		
		cloud_left.setPosition(leftBorder() - cloud_left.getWidth(), MyGame.camera.position.y - (cloud_left.getHeight() / 2) );
		cloud_right.setPosition(rightBorder(), MyGame.camera.position.y - (cloud_left.getHeight() / 2) );
		cloud_top.setPosition(MyGame.camera.position.x - (cloud_top.getWidth() / 2), topBorder());
		cloud_bottom.setPosition(MyGame.camera.position.x - (cloud_top.getWidth() / 2), bottomBorder() - cloud_bottom.getHeight());
		
		
		lCloud = new Cloud(cloud_left, Cloud.LEFT_ALIGN);
		rCloud = new Cloud(cloud_right, Cloud.RIGHT_ALIGN);
		tCloud = new Cloud(cloud_top, Cloud.TOP_ALIGN);
		bCloud = new Cloud(cloud_bottom, Cloud.BOTTOM_ALIGN);
	}
	
	public static float leftBorder(){
		
		float centerX = MyGame.camera.position.x;
		
		return (centerX - HALF_WIDTH);
	}
	
	public static float rightBorder(){
		
		float centerX = MyGame.camera.position.x;
		
		return (centerX + HALF_WIDTH);
	}
	
	public static float topBorder(){
		
		float centerY = MyGame.camera.position.y;
		
		return (centerY + HALF_HEIGHT);
	}
	
	public static float bottomBorder(){
		
		return 0;
	}
	
	public static void showLeft(){
		
		Tween.call(TweenCallBacks.showLeftCloudCallback).start(MyGdxGame.tweenManager);
	}
	
	public static void hideLeft(){
		
		Tween.call(TweenCallBacks.hideLeftCloudCallback).start(MyGdxGame.tweenManager);
	}
	
	public static void showRight(){
		
		Tween.call(TweenCallBacks.showRightCloudCallback).start(MyGdxGame.tweenManager);
	}
	
	public static void hideRight(){
		
		Tween.call(TweenCallBacks.hideRightCloudCallback).start(MyGdxGame.tweenManager);
	}
	
	public static void showTop(){
		
		Tween.call(TweenCallBacks.showTopCloudCallback).start(MyGdxGame.tweenManager);
	}
	
	public static void hideTop(){
		
		Tween.call(TweenCallBacks.hideTopCloudCallback).start(MyGdxGame.tweenManager);
	}
	
	public static void showBottom(){
		
		Tween.call(TweenCallBacks.showBottomCloudCallback).start(MyGdxGame.tweenManager);
	}
	
	public static void hideBottom(){
		
		Tween.call(TweenCallBacks.hideBottomCloudCallback).start(MyGdxGame.tweenManager);
	}

}
