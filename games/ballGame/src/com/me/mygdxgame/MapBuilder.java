package com.me.mygdxgame;

import java.util.ArrayList;

import aurelienribon.tweenengine.Tween;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.randezvou.app.save.InfoDB;
import com.randezvou.app.save.SaveInfo;

public class MapBuilder {
	
	World world;
	
	public MapBuilder(World world){
		
		this.world = world;
	}
	
	private void addGroundTile(String[] objInfo){
		
		float X = Float.parseFloat( objInfo[1] );
		float Y = Float.parseFloat( objInfo[2] );
		float width = Float.parseFloat( objInfo[3] );
		
		GroundTile g = new GroundTile(world, X, Y, width, BodyType.StaticBody);
	}
	
	private void addInclinedTile(String[] objInfo){
		
		float X = Float.parseFloat( objInfo[1] );
		float Y = Float.parseFloat( objInfo[2] );
		float width = Float.parseFloat( objInfo[3] );
		char type = objInfo[4].charAt(0);
		
		InclinedTile inc = new InclinedTile(world, X, Y, width, type, BodyType.StaticBody);
	}
	
	private void addFlyingTile(String[] objInfo){
		
		float startX = Float.parseFloat( objInfo[1] );
		float startY = Float.parseFloat( objInfo[2] );
		float endX = Float.parseFloat( objInfo[3] );
		float endY = Float.parseFloat( objInfo[4] );
		float width = Float.parseFloat( objInfo[5] );
		boolean reverse = Boolean.parseBoolean( objInfo[6] );
		
		FlyingTile fl = new FlyingTile(world, startX, startY, width, endX, endY, reverse);
		MyGame.flyingTiles.add(fl);
	}
	
	private void addCoin(String[] objInfo, int lineNum){
		
		float X = Float.parseFloat( objInfo[1] );
		float Y = Float.parseFloat( objInfo[2] );
		
		OrdinaryCoin c = new OrdinaryCoin(world, X, Y, lineNum);
		MyGame.totalCoins += OrdinaryCoin.coinValue;
		MyGame.coins.add(c);
		
		if(!TweenCallBacks.coinsBouncing){
			
			TweenCallBacks.coinsBouncing = true;
			Tween.call(TweenCallBacks.coinBounceCallBack).start(MyGdxGame.tweenManager);
		}
		
	}
	
	private void addCoin(String[] objInfo, ArrayList<Integer> db, int lineNum){
		
		float X = Float.parseFloat( objInfo[1] );
		float Y = Float.parseFloat( objInfo[2] );
		
		if( !db.contains(lineNum) )
			return;
		
		OrdinaryCoin c = new OrdinaryCoin(world, X, Y, lineNum);
		MyGame.totalCoins += OrdinaryCoin.coinValue;
		MyGame.coins.add(c);
		
		if(!TweenCallBacks.coinsBouncing){
			
			TweenCallBacks.coinsBouncing = true;
			Tween.call(TweenCallBacks.coinBounceCallBack).start(MyGdxGame.tweenManager);
		}
		
	}
	
	private void readLine(String line, int lineNum){
		
		String[] objectInfo = line.split(" ");
		String type = objectInfo[0];
		
		if( type.equals("GROUNDTILE") )
			addGroundTile(objectInfo);
		
		if( type.equals("INCLINEDTILE") )
			addInclinedTile(objectInfo);
		
		if( type.equals("FLYINGTILE") )
			addFlyingTile(objectInfo);
		
		if( type.equals("COIN") )
			addCoin(objectInfo, lineNum);
		
		if( type.equals("BOUNCE") )
			MyGame.ball = new Ball(world, 18.0f, new Vector2(Float.parseFloat(objectInfo[1]), Float.parseFloat(objectInfo[2])) );
		
		if( type.equals("BALLOONY") ){
			
			MyGame.balloony = new Ball(world, 18.0f, new Vector2(Float.parseFloat(objectInfo[1]), Float.parseFloat(objectInfo[2])) );
		}
	}
	
	private void readLine(String line, ArrayList<Integer> db, int lineNum){
		
		String[] objectInfo = line.split(" ");
		String type = objectInfo[0];
		
		if( type.equals("GROUNDTILE") )
			addGroundTile(objectInfo);
		
		if( type.equals("INCLINEDTILE") )
			addInclinedTile(objectInfo);
		
		if( type.equals("FLYINGTILE") )
			addFlyingTile(objectInfo);
		
		if( type.equals("COIN") )
			addCoin(objectInfo, db, lineNum);
		
		if( type.equals("BOUNCE") )
			MyGame.ball = new Ball(world, 18.0f, new Vector2(Float.parseFloat(objectInfo[1]), Float.parseFloat(objectInfo[2])) );
		
		if( type.equals("BALLOONY") ){
			
			MyGame.balloony = new Ball(world, 18.0f, new Vector2(Float.parseFloat(objectInfo[1]), Float.parseFloat(objectInfo[2])) );
		}
	}
	
	public void loadMap(String filename){
		
		FileHandle handle = Gdx.files.internal(filename);
		String data = handle.readString();
		String[] mapLines = data.split("\\r?\\n");
		
		for(int i = 0; i < mapLines.length; i++){
			
			readLine( mapLines[i], i);
		}
		
	}
	
	public void loadMap(String filename, ArrayList<Integer> db){
		
		FileHandle handle = Gdx.files.internal(filename);
		String data = handle.readString();
		String[] mapLines = data.split("\\r?\\n");
		
		for(int i = 0; i < mapLines.length; i++){
			
			readLine( mapLines[i], db, i);
		}
		
	}
	
}
