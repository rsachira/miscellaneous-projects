package com.me.mygdxgame;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Sine;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;

public class TweenCallBacks {
	
	public static boolean coinsBouncing = false;
	public static boolean grassShaking = false;
	
	public static final TweenCallback coinBounceCallBack = new TweenCallback() {
		
		@Override
		public void onEvent(int type, BaseTween<?> source) {
			// TODO Auto-generated method stub
			
			for(int i = 0; i < MyGame.coins.size(); i++){
				
				OrdinaryCoin c = MyGame.coins.get(i);
				
				if(c.shouldDelete){
					
					MyGame.deleteList.add(c.c.coinBody);
					MyGame.coins.remove(i);
				}
				
			}
			
			for(int i = 0; i < MyGame.coins.size(); i++){
				
				OrdinaryCoin c = MyGame.coins.get(i);
				
				if( i == (MyGame.coins.size() - 1) ){
					
					Tween.to(c, 12, 0.5f)
					.target( c.c.coinBody.getPosition().y + 10 )
					.ease(Sine.INOUT)
	                .repeatYoyo(1, 0)
					.setCallback(coinBounceCallBack)
					.start(MyGdxGame.tweenManager);
				}else{
					
					Tween.to(c, 12, 0.5f)
					.target( c.c.coinBody.getPosition().y + 10 )
					.ease(Sine.INOUT)
	                .repeatYoyo(1, 0)
					.start(MyGdxGame.tweenManager);
				}
				
			}// End for
			
		}//End onEvent
	};
	
	public static final TweenCallback windCallback = new TweenCallback() {
	    @Override
	    public void onEvent(int type, BaseTween<?> source) {
	        float d = MathUtils.random() * 0.5f + 0.5f;    // duration
	        float t = -0.5f * 25;    // amplitude
	        
	        for(int i = 0; i < MyGame.grassSprites.size(); i++){
	        	
	        	Sprite grassSprite1 = MyGame.grassSprites.get(i)[0];
	        	Sprite grassSprite2 = MyGame.grassSprites.get(i)[1];
	        	Sprite grassSprite3 = MyGame.grassSprites.get(i)[2];
	        	
	        	if( i == (MyGame.grassSprites.size() - 1) ){
	        		
	        		Timeline.createParallel()
	                .push(Tween.to(grassSprite1, GrassSpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).repeatYoyo(1, 0).setCallback(windCallback))
	                .push(Tween.to(grassSprite2, GrassSpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).delay(d/3).repeatYoyo(1, 0))
	                .push(Tween.to(grassSprite3, GrassSpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).delay(d/3*2).repeatYoyo(1, 0))
	                .start(MyGdxGame.tweenManager);
	        		
	        		return;
	        	}
	        	
	        	Timeline.createParallel()
	            .push(Tween.to(grassSprite1, GrassSpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).repeatYoyo(1, 0))
	            .push(Tween.to(grassSprite2, GrassSpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).delay(d/3).repeatYoyo(1, 0))
	            .push(Tween.to(grassSprite3, GrassSpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).delay(d/3*2).repeatYoyo(1, 0))
	            .start(MyGdxGame.tweenManager);
	        	
	        }
	        
	    }
	};
	
	
		// Cloud callbacks
		public static final TweenCallback showLeftCloudCallback = new TweenCallback() {
			
			@Override
			public void onEvent(int type, BaseTween<?> source) {
				// TODO Auto-generated method stub
				
				if(WindCloud.leftCloudInBound)
					return;
				
				WindCloud.leftCloudInBound = true;
				
				Tween.to(WindCloud.lCloud, 13, 0.5f)
					.target(WindCloud.DELTA_LEFT)
					.ease(Sine.INOUT)
		            .start(MyGdxGame.tweenManager);
				
			}
		};
		
		public static final TweenCallback hideLeftCloudCallback = new TweenCallback() {
			
			@Override
			public void onEvent(int type, BaseTween<?> source) {
				// TODO Auto-generated method stub
				
				if(!WindCloud.leftCloudInBound)
					return;
				
				MyGdxGame.tweenManager.killTarget(WindCloud.DELTA_LEFT, 13);
				
				Tween.to(WindCloud.lCloud, 14, 0.5f)
					.target(0)
					.ease(Sine.INOUT)
		            .setCallback(hideLeftCloudComplete)
		            .start(MyGdxGame.tweenManager);
			}
		};
		
		public static final TweenCallback hideLeftCloudComplete = new TweenCallback() {
			
			@Override
			public void onEvent(int type, BaseTween<?> source) {
				// TODO Auto-generated method stub
				
				WindCloud.leftCloudInBound = false;
			}
		};
		
		
		// Cloud callback : right
		public static final TweenCallback showRightCloudCallback = new TweenCallback() {
			
			@Override
			public void onEvent(int type, BaseTween<?> source) {
				// TODO Auto-generated method stub
				
				if(WindCloud.rightCloudInBound)
					return;
				
				WindCloud.rightCloudInBound = true;
				
				Tween.to(WindCloud.rCloud, 15, 0.5f)
					.target(-1 * WindCloud.DELTA_RIGHT)
					.ease(Sine.INOUT)
		            .start(MyGdxGame.tweenManager);
				
			}
		};
		
		public static final TweenCallback hideRightCloudCallback = new TweenCallback() {
			
			@Override
			public void onEvent(int type, BaseTween<?> source) {
				// TODO Auto-generated method stub
				
				if(!WindCloud.rightCloudInBound)
					return;
				
				Tween.to(WindCloud.rCloud, 16, 0.5f)
					.target(0)
					.ease(Sine.INOUT)
		            .setCallback(hideRightCloudComplete)
		            .start(MyGdxGame.tweenManager);
			}
		};
		
		public static final TweenCallback hideRightCloudComplete = new TweenCallback() {
			
			@Override
			public void onEvent(int type, BaseTween<?> source) {
				// TODO Auto-generated method stub
				
				WindCloud.rightCloudInBound = false;
			}
		};
		
				// Cloud callback : top
				public static final TweenCallback showTopCloudCallback = new TweenCallback() {
					
					@Override
					public void onEvent(int type, BaseTween<?> source) {
						// TODO Auto-generated method stub
						
						if(WindCloud.topCloudInBound)
							return;
						
						WindCloud.topCloudInBound = true;
						
						Tween.to(WindCloud.tCloud, 17, 0.5f)
							.target(WindCloud.DELTA_TOP)
							.ease(Sine.INOUT)
				            .start(MyGdxGame.tweenManager);
						
					}
				};
				
				public static final TweenCallback hideTopCloudCallback = new TweenCallback() {
					
					@Override
					public void onEvent(int type, BaseTween<?> source) {
						// TODO Auto-generated method stub
						
						if(!WindCloud.topCloudInBound)
							return;
						
						Tween.to(WindCloud.tCloud, 18, 0.5f)
							.target(0)
							.ease(Sine.INOUT)
				            .setCallback(hideTopCloudComplete)
				            .start(MyGdxGame.tweenManager);
					}
				};
				
				public static final TweenCallback hideTopCloudComplete = new TweenCallback() {
					
					@Override
					public void onEvent(int type, BaseTween<?> source) {
						// TODO Auto-generated method stub
						
						WindCloud.topCloudInBound = false;
					}
				};
	
				// Cloud callback : bottom
				public static final TweenCallback showBottomCloudCallback = new TweenCallback() {
					
					@Override
					public void onEvent(int type, BaseTween<?> source) {
						// TODO Auto-generated method stub
						
						if(WindCloud.bottomCloudInBound)
							return;
						
						WindCloud.bottomCloudInBound = true;
						
						Tween.to(WindCloud.bCloud, 19, 0.5f)
							.target(WindCloud.DELTA_BOTTOM)
							.ease(Sine.INOUT)
				            .start(MyGdxGame.tweenManager);
						
					}
				};
				
				public static final TweenCallback hideBottomCloudCallback = new TweenCallback() {
					
					@Override
					public void onEvent(int type, BaseTween<?> source) {
						// TODO Auto-generated method stub
						
						if(!WindCloud.bottomCloudInBound)
							return;
						
						Tween.to(WindCloud.bCloud, 20, 0.5f)
							.target(0)
							.ease(Sine.INOUT)
				            .setCallback(hideBottomCloudComplete)
				            .start(MyGdxGame.tweenManager);
					}
				};
				
				public static final TweenCallback hideBottomCloudComplete = new TweenCallback() {
					
					@Override
					public void onEvent(int type, BaseTween<?> source) {
						// TODO Auto-generated method stub
						
						WindCloud.bottomCloudInBound = false;
					}
				};
}
