package com.me.mygdxgame;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class SplitEngine {
	
	public static PolygonShape polyShape = new PolygonShape();
	public static Body body;
	public static boolean drawShape = false;
	
	public static void splitObj(World world, Body sliceBody, Vector2 A, Vector2 B){
		
		Fixture origFixture = sliceBody.getFixtureList().get(0);
		PolygonShape poly = (PolygonShape)origFixture.getShape();
		
		int numVertices = poly.getVertexCount();
		ArrayList<Vector2> verticesVec = getVertices(poly);
		
		ArrayList<Vector2> shape1Vertices = new ArrayList<Vector2>();
		ArrayList<Vector2> shape2Vertices = new ArrayList<Vector2>();
		
		
		
		Vector2 tmp = sliceBody.getLocalPoint(A);
		A.x = tmp.x;
		A.y = tmp.y;
		
		tmp = sliceBody.getLocalPoint(B);
		B.x = tmp.x;
		B.y = tmp.y;
		
		shape1Vertices.add(A);
		shape1Vertices.add(B);
		shape2Vertices.add(A);
		shape2Vertices.add(B);
		
		for (int i = 0; i<numVertices; i++) {
            
			float d = det(A.x,A.y,B.x,B.y,verticesVec.get(i).x,verticesVec.get(i).y);
			
            if (d>0) {
                shape1Vertices.add( verticesVec.get(i) );
            }
            else {
                shape2Vertices.add( verticesVec.get(i) );
            }
        }
        // In order to be able to create the two new shapes, I need to have the vertices arranged in clockwise order.
        // I call my custom method, arrangeClockwise(), which takes as a parameter a vector, representing the coordinates of the shape's vertices and returns a new vector, with the same points arranged clockwise.
        shape1Vertices=arrangeClockwise(shape1Vertices);
        shape2Vertices=arrangeClockwise(shape2Vertices);
        // setting the properties of the two newly created shapes
        BodyDef bodyDef = new BodyDef();
        bodyDef.type=BodyType.DynamicBody;
        bodyDef.position.set( sliceBody.getPosition() );
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = 1;
        fixtureDef.friction = origFixture.getFriction();
        fixtureDef.restitution = 1;
        // creating the first shape, if big enough
        
        Vector2[] vecArray = new Vector2[ shape1Vertices.size() ];
        shape1Vertices.toArray(vecArray);
        
        //if (getArea(shape1Vertices,shape1Vertices.size())>=0.05) {
        	polyShape.set(vecArray);
            //fixtureDef.shape=polyShape;
            body = world.createBody(bodyDef);
            
            drawShape = true; //body.createFixture(polyShape, 1.0f);
        //}
        // creating the second shape, if big enough;
        
       /* Vector2[] vecArray2 = new Vector2[ shape2Vertices.size() ];
        shape2Vertices.toArray(vecArray2);
        
        //if (getArea(shape2Vertices,shape2Vertices.size())>=0.05) {
            polyShape.set(vecArray2);
            fixtureDef.shape=polyShape;
            body=world.createBody(bodyDef);
            body.createFixture(fixtureDef);*/
        //}
		
	}
	
	private static ArrayList<Vector2> getVertices(PolygonShape poly){
		
		ArrayList<Vector2> vertices = new ArrayList<Vector2>();
		
		for(int i = 0; i < poly.getVertexCount(); i++){
			
			Vector2 vertex = new Vector2();
			poly.getVertex(i, vertex);
			
			vertices.add(vertex);
		}
		
		return vertices;
	}
	
	
	
	private static ArrayList<Vector2> arrangeClockwise(ArrayList<Vector2> vec) {
        // The algorithm is simple: 
        // First, it arranges all given points in ascending order, according to their x-coordinate.
        // Secondly, it takes the leftmost and rightmost points (lets call them C and D), and creates tempVec, where the points arranged in clockwise order will be stored.
        // Then, it iterates over the vertices vector, and uses the det() method I talked about earlier. It starts putting the points above CD from the beginning of the vector, and the points below CD from the end of the vector. 
        // That was it!
        int n = vec.size();
        float d;
        int i1 = 1;
        int i2 = n-1;
        ArrayList<Vector2> tempVec = new ArrayList<Vector2>(n);
        Vector2 C;
        Vector2 D;
        
        // sort
        ArrayList<Vector2> v = new ArrayList<Vector2>();
        
        for(int i = 0; i < n; i++)
        	v.add( new Vector2(0,0) );
        
        for(int i = 0; i < vec.size(); i++){
        	
        	int count = 0;
        	
        	for(int j = 0; j < vec.size(); j++){
        		
        		if( comp1( vec.get(i), vec.get(j) ) == 1 )
        			count++;
        	}
        	
        	while( v.get(count).x == vec.get(i).x ){
        		
        		count++;
        	}
        		
        	v.set(count, vec.get(i));
        }
        
        vec = v;
        
        for(int i = 0; i < n; i++)
        	tempVec.add( new Vector2(0,0) );
        
        tempVec.set(0, vec.get(0) );
        C=vec.get(0);
        D=vec.get(n - 1);
        
        for(int i = 1; i < (n - 1); i++){
        	
            d=det(C.x,C.y,D.x,D.y,vec.get(i).x,vec.get(i).y);
            if (d<0) {
                tempVec.set( i1++, vec.get(i) );
            }
            else {
            	tempVec.set( i2--, vec.get(i) );
            }
        }
        tempVec.set( i1, vec.get(n - 1) );
        return tempVec;
    }
	
	private static float getArea(ArrayList<Vector2> vs, int count){
        float area = 0.0f;
        float p1X = 0.0f;
        float p1Y = 0.0f;
        float inv3 = 1.0f/3.0f;
        for (int i = 0; i < count; ++i) {
        	Vector2 p2 = vs.get(i);
            Vector2 p3 = ( (i+1) < count ) ? vs.get( (int)(i+1) ) : vs.get(0);
            float e1X = p2.x - p1X;
            float e1Y = p2.y-p1Y;
            float e2X = p3.x-p1X;
            float e2Y = p3.y-p1Y;
            float D = (e1X * e2Y - e1Y * e2X);
            float triangleArea = 0.5f * D;
            
            area += triangleArea;
        }
        
        return area;
    }
	
	private static int comp1(Vector2 a, Vector2 b) {
        // This is a compare function, used in the arrangeClockwise() method - a fast way to arrange the points in ascending order, according to their x-coordinate.
        if (a.x>b.x) {
            return 1;
        }
        else if (a.x<b.x) {
            return -1;
        }
        return 0;
    }
	
	private static float det(float x1, float y1, float x2, float y2, float x3, float y3) {
        // This is a function which finds the determinant of a 3x3 matrix.
        // If you studied matrices, you'd know that it returns a positive number if three given points are in clockwise order, negative if they are in anti-clockwise order and zero if they lie on the same line.
        // Another useful thing about determinants is that their absolute value is two times the face of the triangle, formed by the three given points.
        return x1*y2+x2*y3+x3*y1-y1*x2-y2*x3-y3*x1;
    }
	
}
