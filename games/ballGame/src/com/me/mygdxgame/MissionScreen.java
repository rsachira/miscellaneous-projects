package com.me.mygdxgame;

import java.util.ArrayList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class MissionScreen implements Screen {
	
	private static final int NUM_MISSIONS = 4;
	
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private Sprite background;
	
	private Game game;
	private Stage stage;
	private Table table;
	
	public MissionScreen(Game g){
		
		game = g;
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		batch.setProjectionMatrix(camera.combined);
		
		
		stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
        
        Table.drawDebug(stage); // This is optional, but enables debug lines for tables.
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
		camera = new OrthographicCamera();
		camera.viewportHeight = 320;  
        camera.viewportWidth = 480;
        
		camera.position.set(camera.viewportWidth * .5f, camera.viewportHeight * .5f, 0f);  
        camera.update();
        
        MyGdxGame.font.setScale(1, 1);
        MyGdxGame.font.scale(-0.75f);
        
        batch = new SpriteBatch();
        stage = new Stage(480, 320, true);
        
        Gdx.input.setInputProcessor(stage);
        
        table = new Table();
        table.setFillParent(true);
        table.left().top();
        
        table.defaults().width(50);
        table.defaults().height(50);
        table.defaults().pad(20);
        
        table.debug();
        table.debugTable();
        
        stage.addActor(table);
        
        generateScreen();
	}
	
	private void generateScreen(){
		
		ArrayList<Level> completedLevels = Storage.generateList();
		
		int incompleteMissions = NUM_MISSIONS - completedLevels.size();
		
		TextButtonStyle completeStyle = generateMissionStyle("finished.png");
		TextButtonStyle incompleteStyle = generateMissionStyle("unfinished.png");
		
		for(int i = 0; i < completedLevels.size(); i++){
			
			Level l = completedLevels.get(i);
			TextButton mish = new TextButton(Integer.toString(l.earnedCoins), completeStyle);
			
			ButtonInput btnInput = new ButtonInput(game, l.levelNo);
			mish.addListener(btnInput);
			
			if( (i % 4) == 0)
				table.row();
			
			table.add(mish);
		}
		
		boolean first_incomplete_mission = true;
		
		for(int i = completedLevels.size(); i < NUM_MISSIONS; i++){
			
			TextButton mish;
			
			if(first_incomplete_mission){
				
				mish = new TextButton("NEW", completeStyle);
				ButtonInput btnInput = new ButtonInput(game, (i + 1));
				mish.addListener(btnInput);
				
				first_incomplete_mission = false;
				
			}else
				mish = new TextButton("LOCKED", incompleteStyle);
			
			if( (i % 4) == 0)
				table.row();
			
			table.add(mish);
		}
		
	}
	
	private TextButtonStyle generateMissionStyle(String pngName){
		
		Texture tex = new Texture(Gdx.files.internal("data/mission_screen/" + pngName));
		TextureRegion texReg = new TextureRegion(tex);
		
		TextButtonStyle style = new TextButtonStyle();
		style.up = new TextureRegionDrawable(texReg);
		style.down = new TextureRegionDrawable(texReg);
		style.font = MyGdxGame.font;
		style.fontColor = Color.BLACK;
		
		return style;
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		batch.dispose();
		stage.dispose();
	}

}
