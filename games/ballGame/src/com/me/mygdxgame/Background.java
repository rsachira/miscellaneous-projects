package com.me.mygdxgame;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Background {
	
	private float backX, backY = 0;
	private Sprite backSprite;
	private float backWidth, backHeight = 0;
	
	private Sprite nextBack;
	private boolean back = false;
	
	public Background(float x, float y, float width, float height, TextureRegion backTex){
		
		backX = x;
		backY = y;
		
		backWidth = width;
		backHeight = height;
		
		backSprite = new Sprite(backTex);
		backSprite.setSize(backWidth, backHeight);
		backSprite.setOrigin(backSprite.getWidth()/2, backSprite.getHeight()/2);
		backSprite.setPosition(backX, backY);
		
		nextBack = new Sprite(backTex);
		nextBack.setSize(backWidth, backHeight);
		nextBack.setOrigin(backSprite.getWidth()/2, backSprite.getHeight()/2);
		nextBack.setPosition(backX + backWidth, backY);
		
	}
	
	public void update(float leftX){
		
		if( (backX + backWidth) < leftX){
			
			backX = backX + backWidth;
			
			backSprite.setPosition(backX, backY);
			nextBack.setPosition(backX + backWidth, backY);
			
			back = false;
		}
		
		if(backX > leftX && !back){
			
			nextBack.setPosition(backX - backWidth, backY);
			back = true;
		}
		
		if(backX < leftX && back){
			
			nextBack.setPosition(backX + backWidth, backY);
			back = false;
		}
		
	}
	
	public void move(float velocity){	// Only in X direction
		
		backSprite.translate(velocity, 0);
		nextBack.translate(velocity, 0);
		
		this.backX = backSprite.getX();
		this.backY = backSprite.getY();
	}

	public Sprite getNextBack() {
		return nextBack;
	}

	public void setNextBack(Sprite nextBack) {
		this.nextBack = nextBack;
	}

	public Sprite getBackSprite() {
		return backSprite;
	}

	public void setBackSprite(Sprite backSprite) {
		this.backSprite = backSprite;
	}
	
	
	
}
