package com.randezvou.app.save;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;
import com.me.mygdxgame.OrdinaryCoin;

public class State {
	
	private ArrayList<Integer> coinsAvailable;
	private int score;
	private float time;
	private Vector2 lastTouchDown;
	
	public State(){
		
		this.coinsAvailable = new ArrayList<Integer>();
		this.score = 0;
		this.time = 0;
		this.lastTouchDown = new Vector2();
	}
	
	public boolean generateState(){
		
		if( !SaveFuncs.getPauseState() )
			return false;
		
		this.coinsAvailable = SaveFuncs.getCoinData();
		this.score = SaveFuncs.getScore();
		this.time = SaveFuncs.getTime();
		this.lastTouchDown = SaveFuncs.getTouchDown();
		
		return true;
	}

	public ArrayList<Integer> getCoinsAvailable() {
		return coinsAvailable;
	}

	public void setCoinsAvailable(ArrayList<OrdinaryCoin> coins) {
		
		for(int i = 0; i < coins.size(); i++){
			OrdinaryCoin c = coins.get(i);
			this.coinsAvailable.add(c.coinNo);
		}
		
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public float getTime() {
		return time;
	}

	public void setTime(float time) {
		this.time = time;
	}

	public Vector2 getLastTouchDown() {
		return lastTouchDown;
	}

	public void setLastTouchDown(Vector2 lastTouchDown) {
		this.lastTouchDown = lastTouchDown;
	}
	
	public void saveState(){
		
		SaveFuncs.setPause(true);
		SaveFuncs.saveAvailableCoins(this.coinsAvailable);
		SaveFuncs.saveScore(this.score);
		SaveFuncs.saveTime( (float)this.time );
		SaveFuncs.saveTouchDown(this.lastTouchDown);
	}
	
}
