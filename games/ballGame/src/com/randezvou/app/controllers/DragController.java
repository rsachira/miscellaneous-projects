package com.randezvou.app.controllers;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.IntIntMap;
import com.me.mygdxgame.Box2DHelper;

public class DragController {
	
	public static final float MAX_MODULUS = 200;
	
	private boolean touched = false;
	private Vector2 initialTouchPos;
	private Vector2 finalTouchPos;
	private Vector3 screenCoords;
	
	public DragController(){
		
		touched = false;
		initialTouchPos = new Vector2(0, 0);
		finalTouchPos = new Vector2(0, 0);
		screenCoords = new Vector3(0, 0, 0);
	}
	
	public void draw(ShapeRenderer sr, Camera camera){
		
		if(!touched)
			return;
		
		sr.begin(ShapeType.Filled);
		sr.setColor(0, 1, 0, 1);
		sr.circle(initialTouchPos.x, initialTouchPos.y, 30);
		sr.end();
		
		sr.begin(ShapeType.Filled);
		sr.setColor(0, 0, 1, 1);
		sr.circle(finalTouchPos.x, finalTouchPos.y, 30);
		sr.end();
		
		sr.setProjectionMatrix(camera.combined);
    	sr.begin(ShapeType.Line);
    	
    	if( Box2DHelper.modulus(initialTouchPos, finalTouchPos) >= MAX_MODULUS)
    		sr.setColor(1, 0, 0, 0);	//Red if max power
    	else
    		sr.setColor(0, 0, 0, 0);	//Black if less than max power
    	
    	sr.line(initialTouchPos.x, initialTouchPos.y, finalTouchPos.x, finalTouchPos.y);
    	sr.end();
	}
	
	public void onTouchDown(Camera camera, int screenX, int screenY){
		
		screenCoords.x = screenX;
		screenCoords.y = screenY;
		screenCoords.z = 0;
		
		camera.unproject(screenCoords);
		initialTouchPos.x = screenCoords.x;
		initialTouchPos.y = screenCoords.y;
		
		touched = true;
	}
	
	public void onTouchUp(){
		touched = false;
	}
	
	public void onTouchDragged(Camera camera, int screenX, int screenY){
		
		screenCoords.x = screenX;
		screenCoords.y = screenY;
		screenCoords.z = 0;
		
		camera.unproject(screenCoords);
		finalTouchPos.x = screenCoords.x;
		finalTouchPos.y = screenCoords.y;
		
		touched = true;
	}
	
	public Vector2 getForce(float MULTIPLIER){
		
		if(!touched)
			return new Vector2(0, 0);
		
		float modulus = Box2DHelper.modulus(finalTouchPos, initialTouchPos);
		if(modulus > MAX_MODULUS)
			modulus = MAX_MODULUS;
		
		float distancePowerUp = (modulus / 200) * 10.0f;
		float TOTAL_MULTIPLIER = MULTIPLIER + distancePowerUp;
		
		float dX = finalTouchPos.x - initialTouchPos.x;
		float dY = finalTouchPos.y - initialTouchPos.y;
		
		Vector2 force = Box2DHelper.unitVector(dX, dY);
		force.x *= TOTAL_MULTIPLIER;
		force.y *= TOTAL_MULTIPLIER;
		
		return force;
	}
}
