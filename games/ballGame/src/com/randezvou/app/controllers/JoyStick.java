package com.randezvou.app.controllers;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.me.mygdxgame.Box2DHelper;
import com.me.mygdxgame.MyGame;
import com.me.mygdxgame.SpriteWrapper;
import com.me.mygdxgame.UserData;

public class JoyStick {
	
	public SpriteWrapper sw;
	public boolean touched = false;
	
	private static final float outerRadius = 30;
	private static final float innerRadius = 10;
	
	private float centerX, centerY = 0;
	private World world;
	public float deltaX, deltaY = 0;	// Amount of X and Y the joystick handle had moved relative to the original position relative to the camera
	private Vector2 centerNow = new Vector2();
	
	private float magnitude = 0;
	private float angle = 0;
	
	private Fixture jFix;
	
	public JoyStick(float X, float Y, Texture handleTex, Texture backTex, World w){
		
		centerX = X;
		centerY = Y;
		world = w;
		
		jFix = createFixture(outerRadius);
		int index = generateSprite(handleTex, backTex);
		
		UserData us = new UserData(UserData.Type.CONTROLLER, 0, index);
		jFix.setUserData(us);
	}
	
	private Fixture createFixture(float radius){
		
		Body b = Box2DHelper.createBody(world, new Vector2(centerX, centerY), 0, BodyType.KinematicBody);
		Shape bShape = Box2DHelper.circle(radius);
		Fixture bFixture = Box2DHelper.createFixture(b, bShape, 0.0f, 0, 0.0f);
		
		return bFixture;
	}
	
	private int generateSprite(Texture handleTex, Texture backTex){
		
		Sprite backSprite = new Sprite(backTex);
		backSprite.setSize(outerRadius * 2, outerRadius * 2);
		backSprite.setOrigin(backSprite.getWidth()/2, backSprite.getHeight()/2);
		backSprite.setPosition(centerX - (backSprite.getWidth() / 2), centerY - (backSprite.getHeight() / 2) );
		
		Sprite handleSprite = new Sprite(handleTex);
		handleSprite.setSize(innerRadius * 2, innerRadius * 2);
		handleSprite.setOrigin(handleSprite.getWidth()/2, handleSprite.getHeight()/2);
		handleSprite.setPosition(centerX - (handleSprite.getWidth() / 2), centerY - (handleSprite.getHeight() / 2) );
		
		ArrayList<Sprite> spriteCol = new ArrayList<Sprite>();
		spriteCol.add(backSprite);
		spriteCol.add(handleSprite);
		
		sw = new SpriteWrapper();
		sw.spriteCollection = spriteCol;
		sw.shouldDraw = true;
		
		return MyGame.joysticks.size() - 1;
	}
	
	public void update(){
		
		if(!touched)
			deltaX = deltaY = 0;
		
		float nowLeft = MyGame.camera.position.x - (MyGame.camera.viewportWidth / 2);
		float nowBottom = MyGame.camera.position.y - (MyGame.camera.viewportHeight / 2);
		
		jFix.getBody().setTransform(nowLeft + centerX, nowBottom + centerY, 0);
		Vector2 pos = jFix.getBody().getPosition();
		
		// Move sprite to right places
		sw.spriteCollection.get(0).setPosition(nowLeft + centerX - (sw.spriteCollection.get(0).getWidth() / 2), nowBottom + centerY - (sw.spriteCollection.get(0).getHeight() / 2) );
		sw.spriteCollection.get(1).setPosition(nowLeft + centerX + deltaX - (sw.spriteCollection.get(1).getWidth() / 2), nowBottom + centerY + deltaY - (sw.spriteCollection.get(1).getHeight() / 2) );
	}
	
	public void touchEventHandler(Vector2 point){
		
		if( jFix.testPoint(point) )
			touched = true;
	}
	
	public void onMove(Vector2 fingerPos){
		
		touchEventHandler(fingerPos);
		
		float nowLeft = MyGame.camera.position.x - (MyGame.camera.viewportWidth / 2);
		float nowBottom = MyGame.camera.position.y - (MyGame.camera.viewportHeight / 2);
		
		centerNow = new Vector2( (nowLeft + centerX), (nowBottom + centerY) );
		float tdeltaX = fingerPos.x - centerNow.x;
		float tdeltaY = fingerPos.y - centerNow.y;
		
		float distance = Box2DHelper.modulus(new Vector2(centerNow.x + tdeltaX,  centerNow.y + tdeltaY), centerNow);
		
		if(distance <= outerRadius){
			
			deltaX = tdeltaX;
			deltaY = tdeltaY;
		}else{
			
			Vector2 deltas = getDeltas(fingerPos, centerNow);
			deltaX = deltas.x;
			deltaY = deltas.y;
			
		}
		
	}
	
	private Vector2 getDeltas(Vector2 fingerPos, Vector2 center){
		
		Vector2 delta = new Vector2();
		float g = (fingerPos.y - center.y) / (fingerPos.x - center.x);
		delta.x = 30 / (float)Math.sqrt( Math.pow(g, 2) + 1 );
		delta.y = (30 * g) / (float)Math.sqrt( Math.pow(g, 2) + 1);
		
		if( (fingerPos.x - center.x) < 0 ){
			
			delta.x *= -1;
			delta.y *= -1;
		}
		
		return delta;
	}
	
	public Vector2 getData(){
		
		magnitude = Box2DHelper.modulus(new Vector2(centerNow.x + deltaX,  centerNow.y + deltaY), centerNow);
		angle = (float)Math.atan2(deltaY, deltaX);
		
		Vector2 data = new Vector2(magnitude, angle);
		
		return data;
	}
	
}
