
package linechartwithhovercoords;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.chart.*;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;

public class LineChartWithHoverCoords extends Application {
  
    private TableView<XYChart.Data> tableView = new TableView<>();
    
    private ObservableList<XYChart.Data> dataList
            = FXCollections.observableArrayList(
                    new XYChart.Data(1, 100),
                    new XYChart.Data(2, 200),
                    new XYChart.Data(3, 50),
                    new XYChart.Data(4, 75),
                    new XYChart.Data(5, 110),
                    new XYChart.Data(6, 300),
                    new XYChart.Data(7, 111),
                    new XYChart.Data(8, 30),
                    new XYChart.Data(9, 75),
                    new XYChart.Data(10, 55),
                    new XYChart.Data(11, 225),
                    new XYChart.Data(12, 99));
    
    private ObservableList<XYChart.Data> dataList2
            = FXCollections.observableArrayList(
                    new XYChart.Data(1, 0),
                    new XYChart.Data(2, 200),
                    new XYChart.Data(3, 50),
                    new XYChart.Data(4, 75),
                    new XYChart.Data(5, 110),
                    new XYChart.Data(6, 300),
                    new XYChart.Data(7, 111),
                    new XYChart.Data(8, 30),
                    new XYChart.Data(9, 75),
                    new XYChart.Data(10, 55),
                    new XYChart.Data(11, 225),
                    new XYChart.Data(12, 100));
    
  @Override public void start(Stage stage) {
    stage.setTitle("Line Chart Sample");
    
    /* Table */
    tableView.setEditable(true);
        Callback<TableColumn, TableCell> cellFactory
                = new Callback<TableColumn, TableCell>() {
                    @Override
                    public TableCell call(TableColumn p) {
                        return 
                                new EditingCell();
                    }
                };
    
    TableColumn columnMonth = new TableColumn("Hours");
        columnMonth.setCellValueFactory(
                new PropertyValueFactory<XYChart.Data, String>("XValue"));

        TableColumn columnValue = new TableColumn("Train 1");
        columnValue.setCellValueFactory(
                new PropertyValueFactory<XYChart.Data, Number>("YValue"));
        
        TableColumn t2 = new TableColumn("Train 2");
        t2.setCellValueFactory(
                new PropertyValueFactory<XYChart.Data, Number>("YValue"));
        

        //--- Add for Editable Cell of Value field, in Number
        columnValue.setCellFactory(cellFactory);
        t2.setCellFactory(cellFactory);
        

        columnValue.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<XYChart.Data, Number>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<XYChart.Data, Number> t) {
                        ((XYChart.Data) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())).setYValue(t.getNewValue());
                    }
                });
        
        t2.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent<XYChart.Data, Number>>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent<XYChart.Data, Number> t) {
                        ((XYChart.Data) t.getTableView().getItems().get(
                                t.getTablePosition().getRow())).setYValue(t.getNewValue());
                    }
                });
    
        tableView.setItems(dataList);
        tableView.getColumns().addAll(columnMonth, columnValue, t2);
        
    final LineChart<Number, Number> lineChart = createChart();
    Label cursorCoords = createCursorGraphCoordsMonitorLabel(lineChart);

    /*stage.setScene(
      new Scene(
        layoutScene(
          lineChart, 
          cursorCoords
        )
      )
    );*/
    
    //lineChart.setMinWidth(1300);
    stage.show();
    
        Group root = new Group();
    
        HBox hBox = new HBox();
        hBox.setSpacing(10);
        hBox.getChildren().addAll(tableView, lineChart, cursorCoords);

        root.getChildren().add(hBox);

        stage.setScene(new Scene(root, 670, 400));
        stage.show();
  }

  private VBox layoutScene(LineChart<Number, Number> lineChart, Label cursorCoords) {
    VBox layout = new VBox(10);
    layout.setPadding(new Insets(10));
    layout.setAlignment(Pos.CENTER);
    layout.getChildren().setAll(
      cursorCoords,
      lineChart
    );
    return layout;
  }
  
  private void addPoint(float x, float y, XYChart.Series series){
      
      XYChart.Data e = new XYChart.Data(x, y);
      dataList2.add(e);
      series.setData(dataList2);
      tableView.setItems(dataList2);
  }
  
  private float timeToCoordinate(int h, int m){
      
      float x = h + (m / 60.0f);
      return x;
  }
  
  private LineChart<Number, Number> createChart() {
    final NumberAxis xAxis = new NumberAxis();
    final NumberAxis yAxis = new NumberAxis();
    xAxis.setLabel("Number of Month");
    final LineChart<Number,Number> lineChart =
        new LineChart<>(xAxis,yAxis);

    lineChart.setTitle("Stock Monitoring, 2010");
    XYChart.Series<Number, Number> series = new XYChart.Series<>(
      "My portfolio", FXCollections.<XYChart.Data<Number, Number>>observableArrayList(
        new XYChart.Data<Number, Number>(1, 23),
        new XYChart.Data<Number, Number>(2, 14),
        new XYChart.Data<Number, Number>(3, 15),
        new XYChart.Data<Number, Number>(4, 24),
        new XYChart.Data<Number, Number>(5, 34),
        new XYChart.Data<Number, Number>(6, 36),
        new XYChart.Data<Number, Number>(7, 22),
        new XYChart.Data<Number, Number>(8, 45),
        new XYChart.Data<Number, Number>(9, 43),
        new XYChart.Data<Number, Number>(10, 17),
        new XYChart.Data<Number, Number>(11, 29),
        new XYChart.Data<Number, Number>(12, 25)
    )
    );
     XYChart.Series series2 = new XYChart.Series(dataList);
     series2.setName("Train 1");
     lineChart.getData().add(series2);
     
     float h = timeToCoordinate(10, 15);
     addPoint(h, 200, series2);
     
     return lineChart;
  }

  private Label createCursorGraphCoordsMonitorLabel(LineChart<Number, Number> lineChart) {
    final Axis<Number> xAxis = lineChart.getXAxis();
    final Axis<Number> yAxis = lineChart.getYAxis();

    final Label cursorCoords = new Label();

    final Node chartBackground = lineChart.lookup(".chart-plot-background");
    for (Node n: chartBackground.getParent().getChildrenUnmodifiable()) {
      if (n != chartBackground && n != xAxis && n != yAxis) {
        n.setMouseTransparent(true);
      }
    }
    //chartBackground.setOnM
    chartBackground.setOnMouseEntered(new EventHandler<MouseEvent>() {
      @Override public void handle(MouseEvent mouseEvent) {
        cursorCoords.setVisible(true);
      }
    });

    chartBackground.setOnMouseMoved(new EventHandler<MouseEvent>() {
      @Override public void handle(MouseEvent mouseEvent) {
        cursorCoords.setText(
          String.format(
            "(%.2f, %.2f)",
            xAxis.getValueForDisplay(mouseEvent.getX()),
            yAxis.getValueForDisplay(mouseEvent.getY())
          )
        );
      }
    });

    chartBackground.setOnMouseExited(new EventHandler<MouseEvent>() {
      @Override public void handle(MouseEvent mouseEvent) {
        cursorCoords.setVisible(false);
      }
    });

    xAxis.setOnMouseEntered(new EventHandler<MouseEvent>() {
      @Override public void handle(MouseEvent mouseEvent) {
        cursorCoords.setVisible(true);
      }
    });

    xAxis.setOnMouseMoved(new EventHandler<MouseEvent>() {
      @Override public void handle(MouseEvent mouseEvent) {
        cursorCoords.setText(
          String.format(
            "x = %.2f",
            xAxis.getValueForDisplay(mouseEvent.getX())
          )
        );
      }
    });

    xAxis.setOnMouseExited(new EventHandler<MouseEvent>() {
      @Override public void handle(MouseEvent mouseEvent) {
        cursorCoords.setVisible(false);
      }
    });

    yAxis.setOnMouseEntered(new EventHandler<MouseEvent>() {
      @Override public void handle(MouseEvent mouseEvent) {
        cursorCoords.setVisible(true);
      }
    });

    yAxis.setOnMouseMoved(new EventHandler<MouseEvent>() {
      @Override public void handle(MouseEvent mouseEvent) {
        cursorCoords.setText(
          String.format(
            "y = %.2f",
            yAxis.getValueForDisplay(mouseEvent.getY())
          )
        );
      }
    });

    yAxis.setOnMouseExited(new EventHandler<MouseEvent>() {
      @Override public void handle(MouseEvent mouseEvent) {
        cursorCoords.setVisible(false);
      }
    });

    return cursorCoords;
  }

  public static void main(String[] args) {
    launch(args);
  }
  
  
  
  class EditingCell extends TableCell<XYChart.Data, Number> {

        private TextField textField;

        public EditingCell() {
        }

        @Override
        public void startEdit() {
            super.startEdit();

            if (textField == null) {
                createTextField();
            }

            setGraphic(textField);
            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
            textField.selectAll();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();

            setText(String.valueOf(getItem()));
            setContentDisplay(ContentDisplay.TEXT_ONLY);
        }

        @Override
        public void updateItem(Number item, boolean empty) {
            super.updateItem(item, empty);

            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                if (isEditing()) {
                    if (textField != null) {
                        textField.setText(getString());
                    }
                    setGraphic(textField);
                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                } else {
                    setText(getString());
                    setContentDisplay(ContentDisplay.TEXT_ONLY);
                }
            }
        }

        private void createTextField() {
            textField = new TextField(getString());
            textField.setMinWidth(this.getWidth() - this.getGraphicTextGap() * 2);
            textField.setOnKeyPressed(new EventHandler<KeyEvent>() {

                @Override
                public void handle(KeyEvent t) {
                    if (t.getCode() == KeyCode.ENTER) {
                        commitEdit(Double.parseDouble(textField.getText()));
                    } else if (t.getCode() == KeyCode.ESCAPE) {
                        cancelEdit();
                    }
                }
            });
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }
  
}