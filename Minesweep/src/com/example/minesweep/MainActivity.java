package com.example.minesweep;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.widget.Button;
import android.widget.RelativeLayout;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		generateMineField(5);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public void generateMineField(int n){
		
		// generate an n x n field => n*n buttons
		
		RelativeLayout rl = (RelativeLayout)findViewById(R.id.container);
		
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		
		
		final int btnWidth = metrics.widthPixels / n;
		final int btnHeight = metrics.heightPixels / n;
		
		for(int i=0; i < n; i++)
			for(int j=0; j < n; j++){
				
				
				Button btn = new Button(this);
				
				btn.setWidth(btnWidth);
				btn.setHeight(btnHeight);
				
				btn.setText("-");
				
				addButtonLayout(btn, RelativeLayout.ALIGN_PARENT_LEFT, btnWidth * j, btnHeight * i, 0, 0);
				
				rl.addView(btn);
				
			}
		
		rl.refreshDrawableState();
	}
	
	private void addButtonLayout(Button button, int centerInParent, int marginLeft, int marginTop, int marginRight, int marginBottom) {
        // Defining the layout parameters of the Button
        RelativeLayout.LayoutParams buttonLayoutParameters = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

        // Add Margin to the LayoutParameters
        buttonLayoutParameters.setMargins(marginLeft, marginTop, marginRight, marginBottom);

        // Add Rule to Layout
        buttonLayoutParameters.addRule(centerInParent);

        // Setting the parameters on the Button
        button.setLayoutParams(buttonLayoutParameters);     
    }

}
