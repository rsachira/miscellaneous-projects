package com.me.mygdxgame;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class Box2DHelper {
	
	public static Body createBody(World world, Vector2 pos, float angle, BodyType bodyType){
	    
		BodyDef bodyDef = new BodyDef(); 
	    bodyDef.type = bodyType;
	    bodyDef.position.set(pos.x, pos.y);
	    bodyDef.angle=angle;

	    return world.createBody(bodyDef);
	}
	
	public static Fixture createFixture(Body body, Shape bodyShape, float density, float restitution, float friction){
		
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.density = density;
		fixtureDef.restitution = restitution;
		fixtureDef.friction = 5;
		fixtureDef.shape = bodyShape;

		return body.createFixture(fixtureDef);
	}
	
	public static Shape circle(float radius){
		
		CircleShape circle = new CircleShape();
		circle.setRadius(radius);
		
		return circle;
	}
	
	public static Shape box(float width, float height){
		
		PolygonShape box = new PolygonShape();
		box.setAsBox( (width / 2f), (height / 2f));
		
		return box;
	}
	
	public static Vector2 unitVector(float deltaX, float deltaY){
		
		float modulus = (float)Math.pow(deltaX, 2) + (float)Math.pow(deltaY, 2);
		modulus = (float)Math.sqrt(modulus);
		
		if(modulus == 0)
			return new Vector2(0, 0);
		
		Vector2 unitVec = new Vector2();
		unitVec.x = deltaX / modulus;
		unitVec.y = deltaY / modulus;
		
		return unitVec;
	}
	
	public static float convertToBox(float x){
		
		return x * 0.01f;
	}
	
	public static float modulus(Vector2 vec1, Vector2 vec2){
		
		float mod = 0.0f;
		mod = (float)Math.pow( (vec1.x - vec2.x), 2) + (float)Math.pow( (vec1.y - vec2.y), 2);
		mod = (float)Math.sqrt(mod);
		
		return mod;
	}
	
}

























