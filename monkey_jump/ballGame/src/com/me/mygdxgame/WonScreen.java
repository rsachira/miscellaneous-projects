package com.me.mygdxgame;

import java.util.ArrayList;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class WonScreen implements Screen {
	
	private OrthographicCamera camera;
	private SpriteBatch batch;
	
	private Sprite background;
	
	private Game game;
	private int earnedCoins;
	private int totalCoins;
	private int coinCount;
	private int currentLevel;
	
	private ArrayList<Level> completedLevels;
	
	private Stage stage;
	
	private int preIndex = 0;
	
	public WonScreen(Game g, int coins, int totalCoins, int level){
		
		game = g;
		earnedCoins = coins;
		this.totalCoins = totalCoins;
		currentLevel = level;
		
		completedLevels = Storage.generateList();
		Level currLevel = generateLevel();
		
		completedLevels = Storage.updateList(currLevel, completedLevels);
		Storage.writeToFile(completedLevels);
	}

	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		batch.setProjectionMatrix(camera.combined);
		
		batch.disableBlending();
		
		batch.begin();
			background.draw(batch);
		batch.end();
		
		batch.enableBlending();
		
		batch.begin();
			//redSquare.draw(batch);
		batch.end();
		
		if(coinCount < earnedCoins){
			
			coinCount++;
			stage.getActors().removeIndex(preIndex);
			preIndex = generateCoins();
		}
		
		stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
		camera = new OrthographicCamera();
		camera.viewportHeight = 320;  
        camera.viewportWidth = 480;
        
		camera.position.set(camera.viewportWidth * .5f, camera.viewportHeight * .5f, 0f);  
        camera.update();
        
        MyGdxGame.font.setScale(1, 1);
        MyGdxGame.font.scale(-0.3f);
        MyGdxGame.font.setColor(Color.WHITE);
        
        batch = new SpriteBatch();
        stage = new Stage(480, 320, true);
        
        Gdx.input.setInputProcessor(stage);
        
        Texture backTex = new Texture(Gdx.files.internal("data/won_screen/won_background.png"));
        TextureRegion texRegion = new TextureRegion(backTex, 0, 0, 512, 341);
        
        background = new Sprite(texRegion);
        background.setSize(480, 320);
        background.setOrigin(background.getWidth() / 2, background.getHeight() / 2);
        background.setPosition(0, 0);
        
        generateRedSquare();
        
        coinCount = 0;
        preIndex = generateCoins();
        
        generateButtons();
	}
	
	private int generateCoins(){
		
		LabelStyle style = new LabelStyle();
		style.font = MyGdxGame.font;
		
		Label coins = new Label(Integer.toString(coinCount), style);
		coins.setPosition(240 - (coins.getWidth() / 2), 160 - ( (coins.getHeight() - 90) / 2) );
		
		stage.addActor(coins);
		
		return stage.getActors().size - 1;
	}
	
	private void generateRedSquare(){
		
		Texture squareTex = new Texture(Gdx.files.internal("data/won_screen/red_square.png"));
		
		Image redSquare = new Image(squareTex);
        redSquare.setSize(48, 24);
        redSquare.setOrigin(redSquare.getWidth() / 2, redSquare.getHeight() / 2);
        redSquare.setPosition(216, 108);
        
        Action scaleAction = Actions.scaleBy(10.0f, 10.0f, 0.2f);
        Action moveAction = Actions.moveTo(210, 145, 0.2f);
        redSquare.addAction(Actions.parallel(scaleAction, moveAction));
        
        stage.addActor(redSquare);
	}
	
	private void generateButtons(){
		
		Texture closeTex = new Texture(Gdx.files.internal("data/won_screen/close.png"));
		TextureRegion closeReg = new TextureRegion(closeTex);
		
		Texture replayTex = new Texture(Gdx.files.internal("data/won_screen/replay.png"));
		TextureRegion replayReg = new TextureRegion(replayTex);
		
		Texture playTex = new Texture(Gdx.files.internal("data/won_screen/play.png"));
		TextureRegion playReg = new TextureRegion(playTex);
		
		TextButtonStyle exitStyle = new TextButtonStyle();
		exitStyle.up = new TextureRegionDrawable(closeReg);
		exitStyle.down = new TextureRegionDrawable(closeReg);
		exitStyle.font = MyGdxGame.font;
		
		TextButtonStyle replayStyle = new TextButtonStyle();
		replayStyle.up = new TextureRegionDrawable(replayReg);
		replayStyle.down = new TextureRegionDrawable(replayReg);
		replayStyle.font = MyGdxGame.font;
		
		TextButtonStyle playStyle = new TextButtonStyle();
		playStyle.up = new TextureRegionDrawable(playReg);
		playStyle.down = new TextureRegionDrawable(playReg);
		playStyle.font = MyGdxGame.font;
		
		ButtonInput replayInput = new ButtonInput(game, currentLevel);
		ButtonInput nextLevelInput = new ButtonInput(game, currentLevel + 1);
		
		TextButton exitGame = new TextButton("", exitStyle);
		stage.addActor(exitGame);
		
		TextButton replayGame = new TextButton("", replayStyle);
		replayGame.addListener(replayInput);
		replayGame.addAction(Actions.moveBy(180, 20));
		stage.addActor(replayGame);
		
		TextButton playGame = new TextButton("", playStyle);
		playGame.addListener(nextLevelInput);
		playGame.addAction(Actions.moveBy(350, 40));
		stage.addActor(playGame);
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		//dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		batch.dispose();
		stage.dispose();
	}
	
	private Level generateLevel(){
		
		Level lvl = new Level();
		lvl.levelNo = currentLevel;
		lvl.earnedCoins = earnedCoins;
		lvl.totalCoins = totalCoins;
		
		return lvl;
	}

}
