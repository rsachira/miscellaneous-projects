package com.me.mygdxgame;

import aurelienribon.tweenengine.TweenManager;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;

public class MyGdxGame extends Game  {
	
	
	public static final String MENU = "menu_v2";
	public static final String GROUND = "ground";
	public static final String COIN = "coin_new";
	
	public static final String GRASS1 = "grass/grass_1";
	public static final String GRASS2 = "grass/grass_2";
	public static final String GRASS3 = "grass/grass_3";
	
	public static final String CLOUD_LEFT = "wind_clouds/wind_cloud_left";
	public static final String CLOUD_RIGHT = "wind_clouds/wind_cloud_right";
	public static final String CLOUD_TOP = "wind_clouds/wind_cloud_top";
	public static final String CLOUD_BOTTOM = "wind_clouds/wind_cloud_bottom";
	
	public static final String BACKGROUND_1 = "backgrounds/background_1";
	public static final String BACKGROUND_2 = "backgrounds/background_2";
	
	public static final String MOUNTAIN_BACK = "backgrounds/mountain_back";
	public static final String MOUNTAIN_TOP = "backgrounds/mountain_top";
	
	public static final String BUTTON_UP = "atlas/button_up";
	public static final String BUTTON_DOWN = "atlas/button_down";
	
	public static final String JOY_STICK = "joystick";
	public static final String JOY_HANDLE = "handle";
	
	public static final String TILE_SET = "tileSet";
	
	public static float resX = 0;
	public static float resY = 0;
	
	public static AssetManager manager;
	public static TweenManager tweenManager;
	public static BitmapFont font;
	
	public MainMenu menuScreen;
	//public WonScreen wonScreen;
	//public MissionScreen missionScreen;
	//MyGame game;
	
	@Override
	public void create() {
		
		resX = Gdx.graphics.getWidth();
		resY = Gdx.graphics.getHeight();
		
		manager = new AssetManager(new ResolutionResolver(MyGdxGame.resX));	//	Declare assets manager
		tweenManager = new TweenManager();
		font = new BitmapFont(Gdx.files.internal("data/fonts/balloons_font.fnt"), false);
		font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		MyGdxGame.manager.load(MENU, Texture.class);
		MyGdxGame.manager.load(GROUND, Texture.class);
		MyGdxGame.manager.load(COIN, Texture.class);
		MyGdxGame.manager.load(TILE_SET, Texture.class);
		
		// Load grass
		MyGdxGame.manager.load(GRASS1, Texture.class);
		MyGdxGame.manager.load(GRASS2, Texture.class);
		MyGdxGame.manager.load(GRASS3, Texture.class);
		
		//Load clouds
		MyGdxGame.manager.load(CLOUD_LEFT, Texture.class);
		MyGdxGame.manager.load(CLOUD_RIGHT, Texture.class);
		MyGdxGame.manager.load(CLOUD_TOP, Texture.class);
		MyGdxGame.manager.load(CLOUD_BOTTOM, Texture.class);
		
		//Load joystick
		MyGdxGame.manager.load(JOY_STICK, Texture.class);
		MyGdxGame.manager.load(JOY_HANDLE, Texture.class);
		
		MyGdxGame.manager.load(BACKGROUND_1, Texture.class);
		//MyGdxGame.manager.load(BACKGROUND_2, Texture.class);
		
		MyGdxGame.manager.load(MOUNTAIN_BACK, Texture.class);
		MyGdxGame.manager.load(MOUNTAIN_TOP, Texture.class);
		
		MyGdxGame.manager.load(BUTTON_UP, Texture.class);
		MyGdxGame.manager.load(BUTTON_DOWN, Texture.class);
        MyGdxGame.manager.finishLoading();
		
		//game = new MyGame(this, 1);
		menuScreen = new MainMenu(this);
		//wonScreen = new WonScreen(this, 50, 50, 1);
		//missionScreen = new MissionScreen(this);
		
		this.setScreen(menuScreen);
		
	}

	@Override
	public void dispose() {

		//game.dispose();
		menuScreen.dispose();
		//wonScreen.dispose();
		//game.dispose();
	}

	@Override
	public void render() {
		
		super.render();
	}

	@Override
	public void resize(int width, int height) {
		
		super.resize(width, height);
	}

	@Override
	public void pause() {
		
		super.pause();
	}

	@Override
	public void resume() {
		
		super.resume();
	}
	
}
