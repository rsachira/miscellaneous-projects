package com.me.mygdxgame;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;

public class Tile {
	
	private float friction;
	private float restitution;
	private float width;
	private float height;
	private float tileAngle;
	private float tileX, tileY = 0;
	private float density;
	
	Body tileBody;
	private Shape tileShape;
	
	public Tile(World world, float tileWidth, float tileHeight, float X, float Y, float angle, BodyType type){
		
		tileX = X;
		tileY = Y;
		tileAngle = angle;
		width = tileWidth;
		height = tileHeight;
		
		density = 1.0f;
		restitution = 0.0f;
		friction = 0.0f;
		
		Vector2 pos = new Vector2(tileX, tileY);
		
		tileBody = Box2DHelper.createBody(world, pos, tileAngle, type);
		tileShape = Box2DHelper.box(width, height);
		tileBody.setBullet(true);
		Fixture tileFixture = Box2DHelper.createFixture(tileBody, tileShape, density, restitution, friction);
		
		//UserData us = new UserData(UserData.Type.TILE, 0, 0);
		//tileFixture.setUserData(us);
	}

}
