package com.randezvou.app.joints;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public class ConnectorClass {
	
	public Body A;
	public Body B;
	public Vector2 cPos;
}
