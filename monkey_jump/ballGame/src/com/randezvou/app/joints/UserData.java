package com.randezvou.app.joints;

public class UserData {
	
	public static int rope = 1;
	public static int ball = 2;
	
	public boolean active;
	public int type;
	
	public boolean isChild = false;
	public boolean isParent = false;
	
	public UserData parent;
	public UserData child;
	
	public UserData(int type, boolean active){
		
		this.type = type;
		this.active = active;
	}
	
}
