atlas_button.png
format: RGBA8888
filter: Linear,Linear
repeat: none
button_up
  rotate: true
  xy: 2, 2
  size: 274, 92
  orig: 294, 104
  offset: 7, 6
  index: -1
button_down
  rotate: true
  xy: 96, 2
  size: 274, 92
  orig: 294, 104
  offset: 7, 6
  index: -1
