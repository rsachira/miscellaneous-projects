package com.me.mygdxgame;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.randezvou.app.joints.UserData;

public class Rope {
	
	private float length;
	private float X;
	private float Y;
	private World world;
	private Body parent;
	
	public Rope(float length, float x, float y, Body parent, World w){
		
		this.length = length;
		this.X = x;
		this.Y = y;
		this.parent = parent;
		this.world = w;
		
		makeRope();
	}
	
	private void makeRope(){
		
		float segmentLength = length / 10;
		Body preBody = parent;
		float cumulatingY = Y;
		
		Vector2 localVec = parent.getLocalPoint(new Vector2(X, Y) );
		
		//Assume that the rope is more or equal to one segment;
		Body seg1 = Box2DHelper.createBody(world, new Vector2(X, Y), 0, BodyType.DynamicBody);
		seg1.setUserData(new UserData(UserData.rope, true) );
		Shape seg1Shape = Box2DHelper.box(5, segmentLength);
		Fixture seg1Fix = Box2DHelper.createFixture(seg1, seg1Shape, 1.0f, 1.0f, 5.0f);
		
		cumulatingY -= segmentLength;
		
		RevoluteJointDef rDef = new RevoluteJointDef();
		rDef.bodyA = parent;
		rDef.localAnchorA.set(localVec);
		rDef.bodyB = seg1;
		rDef.localAnchorB.set(0, (segmentLength / 2) );
		rDef.collideConnected = false;
		ropeDef.referenceAngle = -90 * (Math.PI / 180);
		world.createJoint(rDef);
		
		preBody = seg1;
		
		for(int i = 0; i < 9; i++){
			
			cumulatingY -= segmentLength;
			
			Body b = Box2DHelper.createBody(world, new Vector2(X, cumulatingY), 0, BodyType.DynamicBody);
			
			UserData u = new UserData(UserData.rope, true);
			u.isChild = true;
			u.parent = (UserData)preBody.getUserData();
			
			b.setUserData(u);
			Shape bodyShape = Box2DHelper.box(5, segmentLength);
			Fixture bodyFix = Box2DHelper.createFixture(b, bodyShape, 1.0f, 1.0f, 5.0f);
			
			( (UserData)preBody.getUserData() ).isParent = true;
			( (UserData)preBody.getUserData() ).child = (UserData)b.getUserData();
			
			RevoluteJointDef ropeDef = new RevoluteJointDef();
			ropeDef.bodyA = preBody;
			ropeDef.localAnchorA.set(0, -(segmentLength / 2) );
			ropeDef.bodyB = b;
			ropeDef.localAnchorB.set(0, (segmentLength / 2) );
			ropeDef.collideConnected = false;
			ropeDef.referenceAngle = 0;
			ropeDef.enableLimit = true;
			ropeDef.lowerAngle = -45.0f * ( (float)Math.PI / 180.0f);
			ropeDef.upperAngle = 45.0f * ( (float)Math.PI / 180.0f);
			world.createJoint(ropeDef);
			
			preBody = b;
		}
		
	}
	
	public Joint connect(Body a, Body b, Vector2 cPos){
		
		Vector2 localPosA = a.getLocalPoint(cPos);
		Vector2 localPosB = b.getLocalPoint(cPos);
		
		RevoluteJointDef revDef = new RevoluteJointDef();
		revDef.bodyA = a;
		revDef.bodyB = b;
		revDef.localAnchorA.set(localPosA);
		revDef.localAnchorB.set(localPosB);
		revDef.collideConnected = true;
		
		return world.createJoint(revDef);
	}
	
	public static void deactivateRope(UserData u){
		
		u.active = false;
		UserData tmpU = u;
		while(tmpU.isChild){
			
			tmpU.active = false;
			tmpU = tmpU.parent;
		}
		
		tmpU = u;
		while(true){
			
			tmpU.active = false;
			
			if(tmpU.isParent)
				tmpU = tmpU.child;
			else
				break;
		}
	}
	
}
