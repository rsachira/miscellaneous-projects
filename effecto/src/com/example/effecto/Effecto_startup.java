package com.example.effecto;


import android.net.Uri;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.view.Menu;
import android.view.View.MeasureSpec;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

public class Effecto_startup extends Activity {
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_effecto_startup);
		
		loadGallery();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_effecto_startup, menu);
		return true;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data){
		
		if(requestCode == 1){/* is the data we are looking for */
			
			
			if(resultCode == RESULT_OK){
				
				ImageView img = (ImageView)findViewById(R.id.imageView1);
				img.setImageURI( data.getData() );
				
				Bitmap bmp = getBitmap(img);
				bmp = greyScale(bmp);
				
				img.setImageBitmap( bmp );
			}
			
		}
		
	}
	
	public void loadGallery(){
		
		Intent galleryIntent = new Intent();
		galleryIntent.setAction(Intent.ACTION_PICK);
		galleryIntent.setDataAndType(Uri.parse("content://downloads/external/images/media"), "image/*");
		
		startActivityForResult(galleryIntent, 1);
	}
	
	public Bitmap greyScale(Bitmap bmp){
		
		int width = bmp.getWidth();
		int height = bmp.getHeight();
		
		Bitmap bmpGreyScale = Bitmap.createBitmap(width, height, Config.RGB_565);
		Canvas c = new Canvas(bmpGreyScale);
		
		ColorMatrix cm = new ColorMatrix();
		Paint p = new Paint();
		
		
		cm.setSaturation(0);
		ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
		p.setColorFilter(f);
		
		c.drawBitmap(bmpGreyScale, 0, 0, p);
		
		return bmpGreyScale;
	}
	
	private static Bitmap getBitmap(ImageView v){
		
		v.setDrawingCacheEnabled(true);

		// this is the important code :)  
		// Without it the view will have a dimension of 0,0 and the bitmap will be null          
		v.measure(MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED), 
		            MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
		v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight()); 

		v.buildDrawingCache(true);
		Bitmap b = Bitmap.createBitmap(v.getDrawingCache());
		v.setDrawingCacheEnabled(false); // clear drawing cache
		
		return b;
	}
	

}
