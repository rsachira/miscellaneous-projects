package jumpup;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class StartingClass extends Applet implements Runnable, KeyListener {
	
	enum GameState{
		
		Running, Autopilot, Dead
	}
	
	public static GameState state = GameState.Running;
	
	private static Juno juno;
	private static Background bg1, bg2;
	
	private Image background;
	private URL base;
	private Tile tile1;
	private Coin c1;
	
	public static Image tilegrassbot;
	public static Image coin_front, coin_side;
	
	private ArrayList<Coin> coins = new ArrayList<Coin>();
	private ArrayList<Tile> tiles = new ArrayList<Tile>();
	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
		switch( e.getKeyCode() ){
		
		case KeyEvent.VK_LEFT:
			
			if(state == GameState.Autopilot) break;
			
			System.out.println("Move Left");
			juno.moveLeft();
			juno.setMovingLeft(true);
			break;
			
		case KeyEvent.VK_RIGHT:
			
			if(state == GameState.Autopilot) break;
			
			System.out.println("Move Right");
			juno.moveRight();
			juno.setMovingRight(true);
			break;
		}
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		switch( e.getKeyCode() ){
		
		case KeyEvent.VK_LEFT:
			
			if(state == GameState.Autopilot) break;
			
			System.out.println("stop Move Left");
			juno.stopLeft();
			break;
			
		case KeyEvent.VK_RIGHT:
			
			if(state == GameState.Autopilot) break;
			
			System.out.println("stop Move Right");
			juno.stopRight();
			break;
		}
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		while(true){
			
			// update code start
			juno.update();
			bg1.update();
			bg2.update();
			
			updateTiles();
			updateCoins();
			
			// update code end
			repaint();
			try{ Thread.sleep(17); } catch(InterruptedException e) { e.printStackTrace(); }
		}
		
	}
	
	public void updateCoins(){
		
		for(int i=0; i < coins.size(); i++){
			
			Coin c = coins.get(i);
			c.update();
			
			if( !c.isVisible() )
				coins.remove(i);
		}
		
	}
	
	public void updateTiles(){
		
		for(int i=0; i < tiles.size(); i++){
			
			Tile t = tiles.get(i);
			t.update();
			
			if( !t.isVisible() )
				tiles.remove(i);
		}
		
	}
	
	public void drawCoins(Graphics g){
		
		for(int i=0; i < coins.size(); i++)
			
			coins.get(i).draw(g, this);
	}
	
	public void drawTiles(Graphics g){
		
		for(int i=0; i < tiles.size(); i++)
			
			tiles.get(i).draw(g, this);
	}
	
	@Override
	public void init() {
		// TODO Auto-generated method stub
		
		setSize(480, 800);
		setBackground(Color.BLACK);
		setFocusable(true);
		
		Frame frame = (Frame)this.getParent().getParent();
		frame.setTitle("Jump UP!");
		
		addKeyListener(this);
		
		try{ base = getDocumentBase(); }catch(Exception e){  }
		
		background = getImage(base, "data/background.png");
		tilegrassbot = getImage(base, "data/tilegrassbot.png");
		coin_front = getImage(base, "data/coin-front.png");
		coin_side = getImage(base, "data/coin-side.png");
	}
	
	@Override
	public void start() {
		// TODO Auto-generated method stub
		
		bg1 = new Background(0, 0);
		bg2 = new Background(0, -2160);
		
		juno = new Juno(100, 377, 100);
		//juno.setSpeedY(-5);
		
		try {
            
			loadMap("data/map1.txt");
        
		} catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		
		tile1 = new Tile(0, 5, 1);
		c1 = new Coin(0, 18, 5);
		c1.setCoinY(760);
		
		Thread thread = new Thread(this);
		thread.start();
	}
	
	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void paint(Graphics g) {
		// TODO Auto-generated method stub
		
		g.drawImage(background, bg1.getBgX(), bg1.getBgY(), this);
		g.drawImage(background, bg2.getBgX(), bg2.getBgY(), this);
		
		drawTiles(g);
		drawCoins(g);
		
		juno.draw(g);
		
		g.setColor(Color.BLACK);
		g.drawOval((int)c1.el.getX(), (int)c1.el.getY(), (int)c1.el.getWidth(), (int)c1.el.getHeight());
		g.drawRect( (int)juno.rect.getX(), (int)juno.rect.getY(), (int)juno.rect.getWidth(), (int)juno.rect.getHeight());
	}

	public static Background getBg1() {
		return bg1;
	}

	public static void setBg1(Background bg1) {
		StartingClass.bg1 = bg1;
	}

	public static Background getBg2() {
		return bg2;
	}

	public static void setBg2(Background bg2) {
		StartingClass.bg2 = bg2;
	}

	public static Juno getJuno() {
		return juno;
	}

	public static void setJuno(Juno juno) {
		StartingClass.juno = juno;
	}
	
	private void loadMap(String filename) throws IOException{
		
		ArrayList<String> lines = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		
		int width = 0;
		int height = 0;
		
		while(true){
			
			String line = reader.readLine();
			
			if(line == null){
				
				reader.close();
				break;
			}
			
			if( !line.startsWith("!") ){
				
				lines.add(line);
				width = Math.max(width, line.length());
			}
			
		}
		
		height = lines.size();
		int k = 19;
		
		for(int i=(height - 1); i >= 0; i--){
			
			String line = lines.get(i);
			k--;
			for(int j=0; j < line.length(); j++){
				
				char ch = line.charAt(j);
				addElement(ch, k, j);
			}
			
		}
		
	}
	
	private void addElement(char ch, int i, int j){
		
		switch(ch){
		
		case 'c':
			
			Coin c = new Coin(j, i, 10);
			coins.add(c);
			break;
			
		case 't':
			
			Tile t = new Tile(j, i, 1);
			tiles.add(t);
			break;
		
		}
		
	}
	
}















