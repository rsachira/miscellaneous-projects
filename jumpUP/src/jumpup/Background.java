package jumpup;

public class Background {

	private int bgX, bgY, speedY = 0;
	
	public Background(int x, int y){
		
		bgX = x;
		bgY = y;
		
		speedY = 0;
	}
	
	public void update(){
		
		bgY += speedY;
		
		if(bgY >= 2160)
			bgY += (-4320);
		
	}

	public int getBgX() {
		return bgX;
	}

	public void setBgX(int bgX) {
		this.bgX = bgX;
	}

	public int getBgY() {
		return bgY;
	}

	public void setBgY(int bgY) {
		this.bgY = bgY;
	}

	public int getSpeedY() {
		return speedY;
	}

	public void setSpeedY(int speedY) {
		this.speedY = speedY;
	}
	
	
}
