package jumpup;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.ImageObserver;

public class Tile {
	
	private int tileX, tileY, speedX, speedY, type;
	private boolean visible = false;
	private Background bg = StartingClass.getBg1();
	private Rectangle rect = new Rectangle();
	private Juno juno = StartingClass.getJuno();
	
	public Image tileImage;
	
	public Tile(int x, int y, int typeInt){
		
		tileX = x * 40;
		tileY = y * 40;
		
		type = typeInt;
		visible = true;
		
		// assign image to tileImage
		tileImage = StartingClass.tilegrassbot;
	}
	
	public void update(){
		
		speedY = bg.getSpeedY() * 5;
		
		tileY += speedY;
		
		rect.setBounds( tileX, tileY, 40, 40);
		
		handleCollision(juno.rect);
	}
	
	public void handleCollision(Rectangle r){
		
		if(!visible)
			return;
		
		if( rect.intersects(r) ){
			
			die();
		}
	}
	
	public void die(){
		
		// code for dying
		visible = false;
		juno.setCoins(juno.getCoins() - 5);
	}
	
	public void draw(Graphics g, ImageObserver observer){
		
		if(!visible)
			return;
		
		g.drawImage(tileImage, tileX, tileY, observer);
	}

	public int getTileX() {
		return tileX;
	}

	public void setTileX(int tileX) {
		this.tileX = tileX;
	}

	public int getTileY() {
		return tileY;
	}

	public void setTileY(int tileY) {
		this.tileY = tileY;
	}

	public int getSpeedX() {
		return speedX;
	}

	public void setSpeedX(int speedX) {
		this.speedX = speedX;
	}

	public int getSpeedY() {
		return speedY;
	}

	public void setSpeedY(int speedY) {
		this.speedY = speedY;
	}

	public Image getTileImage() {
		return tileImage;
	}

	public void setTileImage(Image tileImage) {
		this.tileImage = tileImage;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	
}
