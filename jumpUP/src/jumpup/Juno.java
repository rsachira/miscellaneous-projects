package jumpup;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import org.w3c.dom.css.Rect;

import jumpup.StartingClass.GameState;

public class Juno {
	
	final int MOVESPEED = 5;
	private int speedX, speedY, centerX, centerY, coins = 0;
	
	private int width = 100;
	private int height = 100;
	
	private boolean movingLeft, movingRight = false;
	private boolean movingToOtherSide = false;
	
	private Background bg1 = StartingClass.getBg1();
	private Background bg2 = StartingClass.getBg2();
	
	public static Rectangle rect = new Rectangle();
	
	public Juno(int cX, int cY, int initialCoins){
		
		centerX = cX;
		centerY = cY;
		
		coins = initialCoins;
	}
	
	public void update(){
		
		if(speedY > 0)
			centerY += speedY;
		
		if(speedY == 0 || speedY > 0){
			
			//set background speed to 0
			bg1.setSpeedY(0);
			bg2.setSpeedY(0);
		}
		
		if(centerY > 200 && speedY < 0){
			
			centerY += speedY;
		}
		
		if(speedY < 0 && centerY < 200){
			
			//move background with MOVESPEED/5
			bg1.setSpeedY( -speedY/5 );
			bg2.setSpeedY( -speedY/5 );
		}
		
		//update X position
		centerX += speedX;
		
		if(coins == 0 && speedY < 10)
			speedY += 1;
		else{
			
			coins -= 1;
			if(speedY > -5 && speedY != 10)
				speedY -= 1;
		}
		
		if(centerX < 0 && movingToOtherSide == false){
			
			StartingClass.state = GameState.Autopilot;
			
			if( (centerX + width/2) > 0){
				
				// if juno is in the left side move him out of the screen from the left
				moveLeft();
				setMovingLeft(true);
				
			}else{
				
				// if juno is completely out of the screen(left) move him to the right side (still out of the screen)
				stopLeft();
				setCenterX( 480 + (width/2) );
				movingToOtherSide = true;
			}
			
		}
		
		if(centerX > 480 && movingToOtherSide == false){
			
			StartingClass.state = GameState.Autopilot;
			
			if( (centerX - width/2) > 480){
				
				// if juno is in the right side move him out of the screen from the right
				moveRight();
				setMovingRight(true);
				
			}else{
				
				// if juno is completely out of the screen(right) move him to the left side (still out of the screen)
				stopRight();
				setCenterX( -(width/2) );
				movingToOtherSide = true;
			}
			
		}
		
		if(movingToOtherSide == true && centerX <= 0){
			
			if(centerX < 0){	// Move juno into the screen until centerX = 0
				
				moveRight();
				setMovingRight(true);
				
			}else{
				
				//Juno is in place
				stopRight();
				movingToOtherSide = false;
				StartingClass.state = GameState.Running;
			}
		}
		
		if(movingToOtherSide == true && centerX >= 480){
			
			if(centerX > 480){	// Move juno into the screen until centerX = 0
				
				moveLeft();
				setMovingLeft(true);
				
			}else{
				
				//Juno is in place
				stopLeft();
				movingToOtherSide = false;
				StartingClass.state = GameState.Running;
			}
		}
		
		rect.setBounds( (centerX - (width/2) ), (centerY - (height/2) ), width, height);
	}
	
	public void draw(Graphics g){
		
		g.setColor(Color.orange);
		g.fillRect( (centerX - (width/2) ), (centerY - (height/2) ), width, height);
	}
	
	public void moveLeft(){
		
		speedX = -MOVESPEED;
	}
	
	public void moveRight(){
		
		speedX = MOVESPEED;
	}
	
	public void stop(){
		
		if(isMovingLeft() == false && isMovingRight() == false){
			
			speedX = 0;
		}
		
		if(isMovingLeft() == true && isMovingRight() == false){
			
			moveLeft();
		}
		
		if(isMovingLeft() == false && isMovingRight() == true){
			
			moveRight();
		}
	}
	
	public void stopLeft(){
		
		setMovingLeft(false);
		stop();
	}
	
	public void stopRight(){
		
		setMovingRight(false);
		stop();
	}

	public int getSpeedX() {
		return speedX;
	}

	public void setSpeedX(int speedX) {
		this.speedX = speedX;
	}

	public int getSpeedY() {
		return speedY;
	}

	public void setSpeedY(int speedY) {
		this.speedY = speedY;
	}

	public int getCenterX() {
		return centerX;
	}

	public void setCenterX(int centerX) {
		this.centerX = centerX;
	}

	public int getCenterY() {
		return centerY;
	}

	public void setCenterY(int centerY) {
		this.centerY = centerY;
	}

	public int getCoins() {
		return coins;
	}

	public void setCoins(int coins) {
		this.coins = coins;
	}

	public boolean isMovingLeft() {
		return movingLeft;
	}

	public void setMovingLeft(boolean movingLeft) {
		this.movingLeft = movingLeft;
	}

	public boolean isMovingRight() {
		return movingRight;
	}

	public void setMovingRight(boolean movingRight) {
		this.movingRight = movingRight;
	}
	
	

}
