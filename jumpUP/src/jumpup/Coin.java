package jumpup;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.geom.Ellipse2D;
import java.awt.image.ImageObserver;

import kiloboltgame.framework.Animation;

public class Coin {

	private int coinX, coinY, speedX, speedY, width, height, score = 0;
	private boolean visible = false;
	private Background bg = StartingClass.getBg1();
	private Juno juno = StartingClass.getJuno();
	private Animation anim;
	
	public Ellipse2D.Double el = new Ellipse2D.Double();
	
	public Coin(int x, int y, int scoreInt){
		
		width = 40;
		height = 40;
		
		coinX = x * width;
		coinY = y * height;
		score = scoreInt;
		
		anim = new Animation();
		anim.addFrame(StartingClass.coin_front, 100);
		anim.addFrame(StartingClass.coin_side, 100);
		
		visible = true;
	}
	
	public void update(){
		
		speedY = bg.getSpeedY() * 5;
		
		coinY += speedY;
		coinX += speedX;
		
		el.setFrame(coinX, coinY, width, height);
		
		if( collision(juno.rect) ){
			
			die();
			juno.setCoins( juno.getCoins() + score );
			visible = false;
		}
		
		anim.update(10);
	}
	
	public void die(){
		
		// insert code for the die animation and sound
	}
	
	public void draw(Graphics g, ImageObserver obs){
		
		if(!visible)
			return;
		
		g.drawImage(anim.getImage(), coinX, coinY, obs);
	}
	
	private boolean collision(Rectangle rect){
		
		if(!visible)
			return false;
		
		if( el.intersects(rect) )
			return true;
		
		return false;
	}

	public int getCoinX() {
		return coinX;
	}

	public void setCoinX(int coinX) {
		this.coinX = coinX;
	}

	public int getCoinY() {
		return coinY;
	}

	public void setCoinY(int coinY) {
		this.coinY = coinY;
	}

	public int getSpeedX() {
		return speedX;
	}

	public void setSpeedX(int speedX) {
		this.speedX = speedX;
	}

	public int getSpeedY() {
		return speedY;
	}

	public void setSpeedY(int speedY) {
		this.speedY = speedY;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	
}
