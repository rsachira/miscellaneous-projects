package com.me.mygdxgame;

import com.badlogic.gdx.math.Vector2;

public class FlyingTileInfo {
	
	public Vector2 start;
	public Vector2 end;
	public int length;
	public boolean reverse;
}
