package com.me.mygdxgame;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Timeline;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.equations.Sine;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;

public class TweenCallBacks {
	
	public static boolean coinsBouncing = false;
	public static boolean grassShaking = false;
	
	public static final TweenCallback coinBounceCallBack = new TweenCallback() {
		
		@Override
		public void onEvent(int type, BaseTween<?> source) {
			// TODO Auto-generated method stub
			
			for(int i = 0; i < MyGame.coins.size(); i++){
				
				OrdinaryCoin c = MyGame.coins.get(i);
				
				if(c.shouldDelete){
					
					MyGame.deleteList.add(c.c.coinBody);
					MyGame.coins.remove(i);
				}
				
			}
			
			for(int i = 0; i < MyGame.coins.size(); i++){
				
				OrdinaryCoin c = MyGame.coins.get(i);
				
				if( i == (MyGame.coins.size() - 1) ){
					
					Tween.to(c, 12, 0.5f)
					.target( c.c.coinBody.getPosition().y + 10 )
					.ease(Sine.INOUT)
	                .repeatYoyo(1, 0)
					.setCallback(coinBounceCallBack)
					.start(MyGdxGame.tweenManager);
				}else{
					
					Tween.to(c, 12, 0.5f)
					.target( c.c.coinBody.getPosition().y + 10 )
					.ease(Sine.INOUT)
	                .repeatYoyo(1, 0)
					.start(MyGdxGame.tweenManager);
				}
				
			}// End for
			
		}//End onEvent
	};
	
	public static final TweenCallback windCallback = new TweenCallback() {
	    @Override
	    public void onEvent(int type, BaseTween<?> source) {
	        float d = MathUtils.random() * 0.5f + 0.5f;    // duration
	        float t = -0.5f * 25;    // amplitude
	        
	        for(int i = 0; i < MyGame.grassSprites.size(); i++){
	        	
	        	Sprite grassSprite1 = MyGame.grassSprites.get(i)[0];
	        	Sprite grassSprite2 = MyGame.grassSprites.get(i)[1];
	        	Sprite grassSprite3 = MyGame.grassSprites.get(i)[2];
	        	
	        	if( i == (MyGame.grassSprites.size() - 1) ){
	        		
	        		Timeline.createParallel()
	                .push(Tween.to(grassSprite1, GrassSpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).repeatYoyo(1, 0).setCallback(windCallback))
	                .push(Tween.to(grassSprite2, GrassSpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).delay(d/3).repeatYoyo(1, 0))
	                .push(Tween.to(grassSprite3, GrassSpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).delay(d/3*2).repeatYoyo(1, 0))
	                .start(MyGdxGame.tweenManager);
	        		
	        		return;
	        	}
	        	
	        	Timeline.createParallel()
	            .push(Tween.to(grassSprite1, GrassSpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).repeatYoyo(1, 0))
	            .push(Tween.to(grassSprite2, GrassSpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).delay(d/3).repeatYoyo(1, 0))
	            .push(Tween.to(grassSprite3, GrassSpriteAccessor.SKEW_X2X3, d).target(t, t).ease(Sine.INOUT).delay(d/3*2).repeatYoyo(1, 0))
	            .start(MyGdxGame.tweenManager);
	        	
	        }
	        
	    }
	};
}
