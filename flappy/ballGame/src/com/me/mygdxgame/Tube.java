package com.me.mygdxgame;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class Tube{
	
	public static float MIN_DISTANCE_CONSTRAIN = 150.0f;
	public static float MAX_DISTANCE_CONSTRAIN = 150.0f;
	public static float MIN_GAP_CONSTRAIN = 100.0f;
	public static float MAX_DIFFERENCE_CONSTRAIN = 100.0f;
	
	private static float WIDTH = 50.0f;
	
	public Tube(float X, float height, World w){
		
		float y1 = 320 - (height / 2);
		float bottomHeight = (320 - height - MIN_GAP_CONSTRAIN);
		float y2 = bottomHeight / 2;
		
		Body topTube = Box2DHelper.createBody(w, new Vector2(X, y1), 0, BodyType.KinematicBody);
		Shape topTubeShape = Box2DHelper.box(WIDTH, height);
		Fixture topTubeFix = Box2DHelper.createFixture(topTube, topTubeShape, 1.0f, 1.0f, 1.0f);
		
		Body bottomTube = Box2DHelper.createBody(w, new Vector2(X, y2), 0, BodyType.KinematicBody);
		Shape bottomTubeShape = Box2DHelper.box(WIDTH, bottomHeight);
		Fixture bottomTubeFix = Box2DHelper.createFixture(bottomTube, bottomTubeShape, 1.0f, 1.0f, 1.0f);
		
		topTube.setLinearVelocity(-50, 0);
		bottomTube.setLinearVelocity(-50, 0);
	}
}
