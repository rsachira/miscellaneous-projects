package com.me.mygdxgame;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.World;

public class OrdinaryCoin {
	
	public Coin c;
	private static final float height = 25.0f;
	private static final float singleWidth = 25.0f;
	public static final int coinValue = 10;
	
	private ArrayList<Sprite> spriteCol = new ArrayList<Sprite>();
	
	private Texture coinTex;
	
	public int spritesIndex = 0;
	
	public float cX = 0;
	public float cY = 0;
	
	public int coinNo = 0;
	
	// For deleting
	public boolean shouldDelete = false;
	
	
	public OrdinaryCoin(World world, float X, float Y, int coinNo){
		
		cX = X;
		cY = Y;
		
		this.coinNo = coinNo;
		
		cX += Coin.radius;
		
		coinTex = (Texture)MyGdxGame.manager.get(MyGdxGame.COIN);
		coinTex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		
		spritesIndex = generateSprite(singleWidth, height, cX, cY);
		c = new Coin(world, cX, cY, coinValue, spritesIndex);
	}
	
	private int generateSprite(float width, float height, float coinX, float coinY){
		
		Sprite coinSprite = new Sprite(coinTex);
		coinSprite.setSize(width, height);
		coinSprite.setOrigin(coinSprite.getWidth()/2, coinSprite.getHeight()/2);
		coinSprite.setPosition(coinX - (width / 2), coinY - (height / 2));
		
		spriteCol.add(coinSprite);
		
		SpriteWrapper sw = new SpriteWrapper();
		sw.spriteCollection = spriteCol;
		sw.shouldDraw = true;
		
		MyGame.sprites.add(sw);
		
		return MyGame.sprites.size() - 1;
	}
	
	public static void shouldRemove(int sIndex){
		
		for(int i = 0; i < MyGame.coins.size(); i++){
			
			OrdinaryCoin c = MyGame.coins.get(i);
			
			if(c.spritesIndex == sIndex){
				
				c.shouldDelete = true;
				break;
			}
			
		}
		
	}
	
}
