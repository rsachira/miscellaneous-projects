package com.me.mygdxgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;

public class ResolutionResolver implements FileHandleResolver {
	
	private float width;
	
	public ResolutionResolver(float resX){
		
		width = resX;
	}
	
	@Override
	public FileHandle resolve(String fileName) {
		// TODO Auto-generated method stub
		
		FileHandle file;
		String modifiedName = "";
		
		if(width > 800)
			modifiedName = "data/" + fileName + "_.png";
		else
			modifiedName = "data/" + fileName + "_low_res.png";
		
		file = Gdx.files.internal(modifiedName);
		
		if( !file.exists() )
			file = Gdx.files.internal("data/" + fileName + ".png");
		
		return file;
	}

}
