package com.me.mygdxgame;

import java.util.ArrayList;

import aurelienribon.tweenengine.Tween;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

public class GroundTile {
	
	public Tile tile;
	public static final float height = 25.0f;
	public static final float singleWidth = 25.0f;
	
	public float width = 0;
	
	public ArrayList<Sprite> spriteCollection = new ArrayList<Sprite>();
	
	private TextureRegion groundTex;
	private BodyType bodyType;
	
	public GroundTile(World world, float X, float Y, float tileWidth, BodyType type){
		
		float tileX = X;
		float tileY = Y;
		this.width = tileWidth;
		this.bodyType = type;
		
		Texture tileSet = (Texture)MyGdxGame.manager.get(MyGdxGame.TILE_SET);
		tileSet.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		groundTex = new TextureRegion(tileSet, 64, 0, 64, 64);
		
		generateSprite( (width / singleWidth), height, tileX, tileY);
		
		tileX += width / 2;
		tileY -= height / 2;
		
		tile = new Tile(world, width, height, tileX, tileY, 0, type);
	}
	
	private void generateSprite(float indexWidth, float height, float tileX, float tileY){
		
		for(int i = 0; i < indexWidth; i++){
			
			if(bodyType == BodyType.StaticBody){
			
				Sprite grass_1 = new Sprite( (Texture)MyGdxGame.manager.get(MyGdxGame.GRASS1) );
				Sprite grass_2 = new Sprite( (Texture)MyGdxGame.manager.get(MyGdxGame.GRASS2) );
				Sprite grass_3 = new Sprite( (Texture)MyGdxGame.manager.get(MyGdxGame.GRASS3) );
				
				grass_1.setSize(singleWidth, height);
				grass_2.setSize(singleWidth, height);
				grass_3.setSize(singleWidth, height);
				
				grass_1.setOrigin(grass_1.getWidth()/2, grass_1.getHeight()/2);
				grass_2.setOrigin(grass_1.getWidth()/2, grass_1.getHeight()/2);
				grass_3.setOrigin(grass_1.getWidth()/2, grass_1.getHeight()/2);
				
				grass_1.setPosition(tileX + (i * singleWidth), tileY);
				grass_2.setPosition(tileX + (i * singleWidth), tileY);
				grass_3.setPosition(tileX + (i * singleWidth), tileY);
				
				Sprite[] grassSpriteCollection = new Sprite[3];
				grassSpriteCollection[0] = grass_1;
				grassSpriteCollection[1] = grass_2;
				grassSpriteCollection[2] = grass_3;
				
				MyGame.grassSprites.add(grassSpriteCollection);
			}
			
			if(!TweenCallBacks.grassShaking){
				
				Tween.call(TweenCallBacks.windCallback).start(MyGdxGame.tweenManager);
				TweenCallBacks.grassShaking = true;
			}
			
			Sprite tileSprite = new Sprite(groundTex);
			tileSprite.setSize(singleWidth, height);
			tileSprite.setOrigin(tileSprite.getWidth()/2, tileSprite.getHeight()/2);
			tileSprite.setPosition(tileX + (i * singleWidth), tileY - tileSprite.getHeight());
			
			spriteCollection.add(tileSprite);
		}
		
		SpriteWrapper sw = new SpriteWrapper();
		sw.spriteCollection = spriteCollection;
		sw.shouldDraw = true;
		
		MyGame.sprites.add(sw);
	}
	
}
