package com.randezvou.app.map;

import com.badlogic.gdx.physics.box2d.World;
import com.me.mygdxgame.Tube;

public class Builder {
	
	public static float height = Tube.MIN_GAP_CONSTRAIN;
	
	public static float addTubes(float minX, float maxX, World w){
		
		float X = 0.0f;
		float preHeight = 0.0f;
		
		while(minX < maxX){
			
			X = rand(minX + Tube.MIN_DISTANCE_CONSTRAIN, minX + Tube.MAX_DIFFERENCE_CONSTRAIN);
			
			float minHeight;
			float maxHeight;
			
			if( (height - Tube.MAX_DIFFERENCE_CONSTRAIN) <= 0)
				minHeight = Tube.MIN_GAP_CONSTRAIN;
			else
				minHeight = (height - Tube.MAX_DIFFERENCE_CONSTRAIN);
			
			if( (height + Tube.MAX_DIFFERENCE_CONSTRAIN) >= 320)
				maxHeight = 320 - 2 * Tube.MIN_GAP_CONSTRAIN;
			else
				maxHeight = (height + Tube.MAX_DIFFERENCE_CONSTRAIN);
			
			height = rand(minHeight, maxHeight);
			
			Tube t = new Tube(X, height, w);
			
			if(X > maxX)
				return preHeight;
			
			minX = X;
			preHeight = height;
		}
		
		return preHeight;
	}
	
	public static float rand(float Min, float Max){
		
		return Min + (int)(Math.random() * ((Max - Min) + 1));
	}
	
}
