package com.randezvou.app.save;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Json;
import com.me.mygdxgame.OrdinaryCoin;

public class SaveFuncs {
	
	public static void saveAvailableCoins(ArrayList<Integer> availableCoins){
		
		String data = "";
		for(int i = 0; i < availableCoins.size(); i++){
			if(i > 0)
				data += ":";
			
			int c = availableCoins.get(i);
			data += Integer.toString(c);
		}
		
		Preferences prefs = Gdx.app.getPreferences("save_data");
		prefs.putString("coins_save_data", data);
		prefs.flush();
	}
	
	public static void setPause(boolean state){
		
		Preferences prefs = Gdx.app.getPreferences("save_data");
		prefs.clear();
		
		prefs.putBoolean("pause", state);
		prefs.flush();
	}
	
	public static boolean getPauseState(){
		
		Preferences prefs = Gdx.app.getPreferences("save_data");
		return prefs.getBoolean("pause", false);
	}
	
	public static ArrayList<Integer> getCoinData(){
		ArrayList<Integer> db = new ArrayList<Integer>();
		
		Preferences prefs = Gdx.app.getPreferences("save_data");
		String data = prefs.getString("coins_save_data", "");
		
		if( data.equals("") )
			return null;
		
		String[] coins = data.split(":");
		for(int i = 0; i < coins.length; i++){
			
			String c = coins[i];
			db.add( Integer.parseInt(c) );
		}
		
		return db;
	}
	
	public static int getScore(){
		
		Preferences prefs = Gdx.app.getPreferences("save_data");
		int score = prefs.getInteger("score_save_data", 0);
		return score;
	}
	
	public static float getTime(){
		
		Preferences prefs = Gdx.app.getPreferences("save_data");
		float time = prefs.getFloat("time_save_data", 0.0f);
		return time;
	}
	
	public static Vector2 getTouchDown(){
		
		Preferences prefs = Gdx.app.getPreferences("save_data");
		float touchX = prefs.getFloat("touchx_save_data", 0.0f);
		float touchY = prefs.getFloat("touchy_save_data", 0.0f);
		
		Vector2 touchDown = new Vector2(touchX, touchY);
		return touchDown;
	}
	
	public static void saveScore(int score){
		
		Preferences prefs = Gdx.app.getPreferences("save_data");
		prefs.putInteger("score_save_data", score);
		prefs.flush();
	}
	
	public static void saveTime(float time){
		
		Preferences prefs = Gdx.app.getPreferences("save_data");
		prefs.putFloat("time_save_data", time);
		prefs.flush();
	}
	
	public static void saveTouchDown(Vector2 touchDown){
		
		Preferences prefs = Gdx.app.getPreferences("save_data");
		prefs.putFloat("touchx_save_data", touchDown.x);
		prefs.putFloat("touchy_save_data", touchDown.y);
		prefs.flush();
	}
	
}
