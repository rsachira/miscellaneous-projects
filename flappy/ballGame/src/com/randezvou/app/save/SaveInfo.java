package com.randezvou.app.save;

public class SaveInfo {
	
	public String type;
	public double X;
	public double Y;
	
	public SaveInfo(String t, double x, double y){
		
		type = t;
		X = x;
		Y = y;
	}
	
}
