package com.example.opengltest;
import android.content.Context;
import android.opengl.GLSurfaceView;


public class MyGLSurfaceView extends GLSurfaceView{

	public MyGLSurfaceView(Context context){
		
		super(context);
		super.setEGLContextClientVersion(2);
		//super.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
		
		setRenderer( new MyRenderer() );
	}
}
