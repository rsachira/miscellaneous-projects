package com.example.opengltest;

import android.graphics.Canvas.VertexMode;
import android.opengl.GLES20;

public class Shader {
	
	
	private static final String vertexShaderCode = 
			"attribute vec4 vPosition;" +
			"void main(){" +
				"gl_Position = vPosition;" +
			"}";
	
	private static final String fragmentShaderCode = 
			"precision mediump float;" +
			"uniform vec4 Color;" +
			"void main(){" +
				"gl_Fragcolor = vColor;" +
			"}";
	
	public static int loadShader(int type){
		
		String code = "";
		
		if(type == GLES20.GL_VERTEX_SHADER)
			code = vertexShaderCode;
		
		if(type == GLES20.GL_FRAGMENT_SHADER)
			code = fragmentShaderCode;
		
		int shader = GLES20.glCreateShader(type);
		GLES20.glShaderSource(shader, code);
		GLES20.glCompileShader(shader);
		
		return shader;
	}
	
}
