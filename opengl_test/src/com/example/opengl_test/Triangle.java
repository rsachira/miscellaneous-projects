package com.example.opengl_test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import android.graphics.Point;

public class Triangle extends Shape {
	
	
	public Triangle(float width, float height, float X, float Y){
		
		this.width = width;
		this.height = height;
		this.x = X;
		this.y = Y;
		
		this.vertexCount = 3;
		
		float[] triangleColor = { 0.63671875f, 0.76953125f, 0.22265625f, 1.0f };
		this.color = triangleColor;
		
		this.plotTriangle();
		
	}
	
	private void plotTriangle(){
		
		float triangleCoordinates[] = new float[9];/* coordinates in counter clockwise order */
		
		triangleCoordinates[0] = this.x;
		triangleCoordinates[1] = this.y;
		triangleCoordinates[2] = 0.0f;
		
		triangleCoordinates[3] = this.x;
		triangleCoordinates[4] = this.y + this.height;
		triangleCoordinates[5] = 0.0f;
		
		triangleCoordinates[6] = this.x + this.width;
		triangleCoordinates[7] = this.y + this.height;
		triangleCoordinates[8] = 0.0f;
		
		
		ByteBuffer bb = ByteBuffer.allocate(triangleCoordinates.length * 4); /* allocate 4 bytes per float */
		bb.order( ByteOrder.nativeOrder() ); /* use the device's native byte order */
		
		this.vertexBuffer = bb.asFloatBuffer();/* create float point buffer from byte buffer */
		this.vertexBuffer.put(triangleCoordinates);/* add the coordinates to the float buffer */
		this.vertexBuffer.position(0);/* set the buffer to read the first coordinate */
		
		
		/* order to draw vertices */
		short drawOder[] = { 0, 1, 2 };
		
		/* initialize byte buffer for the draw list :- allocate 2 bytes per short */
		ByteBuffer blb = ByteBuffer.allocate(drawOder.length * 2);
		
		blb.order( ByteOrder.nativeOrder() );
		
		this.drawListBuffer = blb.asShortBuffer();
		this.drawListBuffer.put(drawOder);
		this.drawListBuffer.position(0);
		
	}
	
}
