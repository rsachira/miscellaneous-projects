package com.example.opengl_test;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.EGLConfig;
import android.opengl.GLES10;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;


public class MyGL20Renderer implements GLSurfaceView.Renderer {
	
	
	public void onSurfaceCreated(GL10 unused, javax.microedition.khronos.egl.EGLConfig config){
		
		GLES10.glClearColor(0f , 0f, 0f, 1.0f);
		
		Triangle tri = new Triangle(0.2f, 0.2f, (float)0, (float)0);
		int mProgram = Shape.shaderProgram();
		
		tri.Draw(mProgram);
		
	}
	
	public void onDrawFrame(GL10 unused){
		
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);
	}
	
	public void onSurfaceChanged(GL10 unused, int width, int height){
		
		GLES20.glViewport(0, 0, width, height);
	}
	
}
