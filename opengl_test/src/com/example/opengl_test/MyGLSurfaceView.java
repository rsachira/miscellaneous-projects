package com.example.opengl_test;
import android.content.Context;
import android.opengl.GLSurfaceView;


public class MyGLSurfaceView extends GLSurfaceView {
	
	
	public MyGLSurfaceView(Context context){
		
		super(context);
		setRenderer( new MyGL20Renderer() );
		//setEGLContextClientVersion(2);
		setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
	}
	
}
