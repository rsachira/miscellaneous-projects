package com.example.opengl_test;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.graphics.Point;
import android.opengl.GLES20;

public abstract class Shape {
	
	public float width;
	public float height;
	
	protected float x;
	protected float y;
	
	public float[] color;
	
	protected int vertexCount;
	public static final int vertexStride = Shape.COORDS_PER_VERTEX * 4;
	
	protected FloatBuffer vertexBuffer;
	protected ShortBuffer drawListBuffer;
	
	public static final String vertexShaderCode = 
			"attribute vec4 vPosition;" +
			"void main() {" +
				"  gl_Position = vPosition;" +
			"}"
			;
	public static final String fragmentShaderCode = 
			"precision mediump float;" +
			"uniform vec4 vColor;" +
			"void main() {" +
				"  gl_FragColor = vColor;" +
			"}"
			;
	
	public static final int COORDS_PER_VERTEX = 3;
	
	public static final int loadShader(int type, String shaderCode){
		
		/* create shader with the given type -> GLES20.GL_VERTEX_SHADER or GLES20.GL_FRAGMENT_SHADER */
		int shader = GLES20.glCreateShader(type);
		
		/* add the source code to the shader and compile it */
		GLES20.glShaderSource(shader, shaderCode);
		GLES20.glCompileShader(shader);
		
		return shader;
	}
	
	public static final int shaderProgram(){
		
		int vertexShader = Shape.loadShader(GLES20.GL_VERTEX_SHADER, Shape.vertexShaderCode);
		int fragmentShader = Shape.loadShader(GLES20.GL_VERTEX_SHADER, Shape.fragmentShaderCode);
		
		int mProgram = GLES20.glCreateProgram();
		
		GLES20.glAttachShader(mProgram, vertexShader);
		GLES20.glAttachShader(mProgram, fragmentShader);
		
		GLES20.glLinkProgram(mProgram);
		
		return mProgram;
	}
	
	static float triangleCoords[] = { // in counterclockwise order:
        0.0f,  0.622008459f, 0.0f,   // top
       -0.5f, -0.311004243f, 0.0f,   // bottom left
        0.5f, -0.311004243f, 0.0f    // bottom right
   };
	
	private FloatBuffer vvertexBuffer;
	
	public final void Draw(int mProgram){
		
		GLES20.glUseProgram(mProgram);/* add shader program to the GLES 20 environment*/
		
		/* get a handle to vertex shader's vPosition member */
		int mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");
		
		/* enable a handle to the triangle vertices */
		GLES20.glEnableVertexAttribArray(mPositionHandle);
		
		
		ByteBuffer bb = ByteBuffer.allocateDirect(
		        // (# of coordinate values * 4 bytes per float)
				triangleCoords.length * 4);
		        bb.order(ByteOrder.nativeOrder());
		        vvertexBuffer = bb.asFloatBuffer();
		        vvertexBuffer.put(triangleCoords);
		        vvertexBuffer.position(0);
		
		
		
		/* prepare triangle coordinate data */
		GLES20.glVertexAttribPointer(mPositionHandle, Shape.COORDS_PER_VERTEX, GLES20.GL_FLOAT, false, Shape.vertexStride, vvertexBuffer);
		
		/* get a handle to fragment shader's vColor member */
		int mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");
		
		/* set the color for drawing the shape */
		GLES20.glUniform4fv(mColorHandle, 1, this.color, 0);
		
		/* draw the triangle */
		GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, this.vertexCount);
		
		/* disable vertex array */
		GLES20.glDisableVertexAttribArray(mPositionHandle);
		
	}
	
}





















